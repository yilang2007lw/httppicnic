/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "file_util.h"

static FileInfo * file_info_new (const char *path);
static void file_info_free (FileInfo *info);
static char *get_file_time (struct stat buf);
static char *get_file_type (struct stat buf);
static char *get_file_mimetype (const char *path);
static char *get_file_permission (struct stat buf);

static int create_directory (const char *cwd, const char *dirname, const char *mode);
static int create_file (const char *cwd, const char *filename, const char *mode);
static int delete_directory (const char *dirpath);
static int delete_file (const char *filepath);
static int delete_file_directories (const char *cwd, const char **names);
static int delete_files (const char *cwd, const char **filenames);
static int move_files (const char *destdir, const char *cwd, const char **filenames);
static int copy_files (const char *destdir, const char *cwd, const char **filenames);
static int rename_file (const char *destname, const char *cwd, const char *filename);
static int compress_files (const char *cwd, const char **filenames, char *result);

static FileInfo *
file_info_new (const char *path)
{
    FileInfo *info = NULL;

    info = (FileInfo *) malloc (sizeof (FileInfo));
    if (info == NULL) {
        perror ("malloc");
        return NULL;
    }

    struct stat buf;
    if (lstat (path, &buf) == -1) {
        perror ("lstat");
        free (info);
        info = NULL;
        return NULL;
    }

    char *name = strrchr (path, '/');
    name++;
    info->name = strndup (name, strlen (name));
    info->path = strndup (path, strlen (path));
    info->inode = buf.st_ino;
    info->size = buf.st_size;
    info->time = get_file_time (buf);
    info->type = get_file_type (buf);
    info->permission = get_file_permission (buf);
    info->mime = get_file_mimetype (path);
    info->next = NULL;

    return info;
}

static void 
file_info_free (FileInfo *info)
{
    if (info == NULL) {
        return ;
    }
    if (info->name != NULL) {
        free (info->name);
        info->name = NULL;
    }
    if (info->type != NULL) {
        free (info->type);
        info->type = NULL;
    }
    if (info->path != NULL) {
        free (info->path);
        info->path = NULL;
    }
    if (info->time != NULL) {
        free (info->time);
        info->time = NULL;
    }
    if (info->permission != NULL) {
        free (info->permission);
        info->permission = NULL;
    }
    if (info->mime != NULL) {
        free (info->mime);
        info->mime = NULL;
    }
    info->next = NULL;

    free (info);
    info = NULL;
}

FileInfoList 
file_info_list_new (const char *path)
{
    FileInfoList list = NULL;

    list = file_info_new (path);

    DIR *dir = opendir (path);
    if (dir != NULL) {
        FileInfo *tmp =  list;

        struct dirent *dirp;
        while ((dirp = readdir(dir)) != NULL) {

            if (strcmp (dirp->d_name, ".") == 0 || strcmp (dirp->d_name, "..") == 0){
                continue;
            }

            char *new_path = (char *) malloc (256);
            if (new_path == NULL) {
                perror ("malloc");
                continue;
            }
            memset (new_path, 0, 256);

            strncpy (new_path, path, strlen (path) + 1);
            strncat (new_path, "/", 1);
            strncat (new_path, dirp->d_name, strlen (dirp->d_name));

            FileInfo *pfile = file_info_new (new_path);
            tmp->next = pfile;
            tmp = tmp->next;
            free (new_path);
        }
    } 

    return list;
}

void
file_info_list_free (FileInfoList list)
{
    FileInfo *q;
    while (list != NULL) {
        q = list;
        list = list->next;
        file_info_free (q);
    }
}

static char *
get_file_time (struct stat buf)
{
    char *time =  NULL;

    char *rtime = ctime (&buf.st_ctime);
    time = strndup (rtime, strlen (rtime) -1);
    return time;
}

static char *
get_file_type (struct stat buf)
{
    char *type = NULL;

    type = (char *) malloc (20);
    if (type == NULL) {
        perror ("malloc");
        return NULL;
    }
    memset (type, 0, 20);

    if (S_ISDIR (buf.st_mode)) {
        strncpy (type, "directory", 10);
    } else if (S_ISREG (buf.st_mode)) {
        strncpy (type, "regular", 8);
    } else if (S_ISCHR (buf.st_mode)) {
        strncpy (type, "char", 5);
    } else if (S_ISBLK (buf.st_mode)) {
        strncpy (type, "blk", 4);
    } else if (S_ISFIFO (buf.st_mode)) {
        strncpy (type, "fifo", 5);
    } else if (S_ISLNK (buf.st_mode)) {
        strncpy (type, "link", 5);
    } else if (S_ISSOCK (buf.st_mode)) {
        strncpy (type, "socket", 7);
    } else {
        strncpy (type, "unknown", 8);    
    }

    return type;
}

static char *
get_file_permission (struct stat buf)
{
    char *permission = NULL;

    permission = (char *) malloc (10);
    if (permission == NULL) {
        perror ("malloc");
        return NULL;
    }
    memset (permission, 0, 10);

    if (S_IRUSR & buf.st_mode) {
        strncat (permission, "r", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IWUSR & buf.st_mode) { 
        strncat (permission, "w", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IXUSR & buf.st_mode) {
        strncat (permission, "x", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IRGRP & buf.st_mode) {
        strncat (permission, "r", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IWGRP & buf.st_mode) {
        strncat (permission, "w", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IXGRP & buf.st_mode) {
        strncat (permission, "x", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IROTH & buf.st_mode) {
        strncat (permission, "r", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IWOTH & buf.st_mode) {
        strncat (permission, "w", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IXOTH & buf.st_mode) {
        strncat (permission, "x", 1);
    } else {
        strncat (permission, "-", 1);
    }

    return permission;
}

static char *
get_file_mimetype (const char *path)
{
    char *mime = NULL;
    char *command = NULL;
    char *output = NULL;
    FILE *stream = NULL;

    if (path == NULL) {
        perror ("get file mimetype");
        return NULL;
    }

    command = (char *) malloc (256);
    if (command == NULL) {
        perror ("malloc");
        goto out;
    }
    memset (command, 0, 256);
    snprintf (command, 256, "%s %s", "file -i ", path);

    output = (char *) malloc (1024);
    if (output == NULL) {
        perror ("malloc");
        goto out;
    }
    memset (output, 0, 1024);

    stream = popen (command, "r");
    if (stream == NULL) {
        perror ("popen");
        goto out;
    }
    if (fgets (output, 1024, stream) == NULL) {
        perror ("fgets");
        goto out;
    }
    char *mime_charset = strrchr (output, ':');
    if (mime_charset == NULL) {
        perror ("get mime charset index");
        goto out;
    }
    char *charset = strrchr (mime_charset, ';');
    if (charset == NULL) {
        perror ("get charset index");
        goto out;
    }

    mime_charset++;
    mime_charset++;

    mime = strndup (mime_charset, strlen (mime_charset) - strlen (charset));

    free (output);
    output = NULL;
    free (command);
    command = NULL;
    pclose (stream);

    return mime;
out:
    if (command != NULL) {
        free (command);
        command = NULL;
    }
    if (output != NULL) {
        free (output);
        output = NULL;
    }
    if (stream != NULL) {
        pclose (stream);
    }
    return NULL;
}

void 
file_info_list_print (FileInfoList list)
{
    while (list != NULL) {
        printf ("name:%s\n", list->name);
        printf ("type:%s\n", list->type);
        printf ("path:%s\n", list->path);
        printf ("size:%jd\n", (intmax_t)list->size);
        printf ("permission:%s\n", list->permission);
        printf ("mime:%s\n", list->mime);
        printf ("time:%s\n", list->time);
        list = list->next;
    }
}

static int 
create_directory (const char *cwd, const char *dirname, const char *mode)
{
    int status = -1;
    if (cwd == NULL || dirname == NULL) {
        fprintf (stderr, "cwd or dirname NULL");
        return status;
    }

    char *path = (char *) malloc (256);
    if (path == NULL) {
        fprintf (stderr, "create direcotry:malloc");
        return status;
    }
    memset (path, 0, 256);
    snprintf (path, strlen (cwd) + strlen (dirname) + 1, "%s%s", cwd, dirname); 
    if (mode == NULL) {
        mode = "0644";
    }
    if (mkdir (path, mode) == 0) {
       status = 0; 
    } else {
        fprintf (stderr, strerror (errno));
    }
    free (path);
    path = NULL;

    return status;
}

static int 
create_file (const char *cwd, const char *filename, const char *mode)
{
    int status = -1;
    if (cwd == NULL || filename == NULL) {
        fprintf (stderr, "cwd or filename NULL");
        return status;
    }

    char *path = (char *) malloc (256);
    if (path == NULL) {
        fprintf (stderr, "create direcotry:malloc");
        return status;
    }
    memset (path, 0, 256);
    snprintf (path, strlen (cwd) + strlen (filename) + 1, "%s%s", cwd, filename); 
    if (mode == NULL) {
        mode = "0644";
    }
    if (creat (path, mode) == 0) {
       status = 0; 
    } else {
        fprintf (stderr, strerror (errno));
    }
    free (path);
    path = NULL;

    return status;
}

static int 
delete_directory (const char *dirpath)
{
    int status = -1;

    if (dirpath == NULL) {
        fprintf (stderr, "delete directory:dirpath NULL\n");
        return status;
    }
    if (rmdir (dirpath) != 0) {
        fprintf (stderr, "delete directory:%s\n", strerror (errno));
    } else {
        status = 0;
    }

    return status;
}

static int 
delete_file (const char *filepath)
{
    int status = -1;
    if (filepath == NULL) {
        fprintf (stderr, "delete file:filepath NULL\n");
        return status;
    }

    if (unlink (filepath) != 0) {
        fprintf (stderr, "delete file:%s\n", strerror (errno));
    } else {
        status = 0;
    }

    return status;
}

static int
delete_file_directories (const char *cwd, const char **names)
{
    int status = -1;
    char *path = NULL;
    char *p = NULL;

    if (cwd == NULL || names == NULL) {
        fprintf (stderr, "delete file direcotries:cwd or names NULL");
        return status;
    }

    path = (char *) malloc (256);
    if (path == NULL) {
        fprintf (stderr, "delete file directories:malloc");
        return status;
    }
    for (p = *names; p != NULL; p++) {
        memset (path, 0, 256);
        strcat (path, cwd);
        strcat (path, p);

        struct stat buf;
        if (lstat (path, &buf) != 0) {
            fprintf (stderr, "delete file directories:%s\n", path);
            continue;
        }
        if (S_ISDIR (buf.st_mode)) {
            delete_directory (path);
        } else {
            delete_file (path);
        }
    }
    status = 0;
    free (path);
    path = NULL;

    return status;
}

static int 
move_files (const char *destdir, const char *cwd, const char **filenames)
{
    int status = -1;

    return status;
}

static int 
copy_files (const char *destdir, const char *cwd, const char **filenames)
{
    int status = -1;

    return status;
}

static int 
rename_file (const char *destname, const char *cwd, const char *filename)
{
    int status = -1;
    char *oldpath = NULL;
    char *newpath = NULL;

    if (destname == NULL || cwd == NULL || filename == NULL) {
        fprintf (stderr, "rename file:destname or cwd or filename NULL\n");
        goto out;
    }

    oldpath = (char *) malloc (256);
    if (oldpath == NULL) {
        fprintf (stderr, "rename file:malloc");
        goto out;
    }
    memset (oldpath, 0, 256);
    strcat (oldpath, cwd);
    strcat (oldpath, filename);

    newpath = (char *) malloc (256);
    if (newpath == NULL) {
        fprintf (stderr, "rename file:malloc");
        goto out;
    }
    strcat (newpath, cwd);
    strcat (newpath, destname);

    if (access (newpath, F_OK) == 0) {
        fprintf (stderr, "rename file:already exists");
        goto out;
    }

    if (rename (oldpath, newpath) != 0) {
        fprintf (stderr, "rename file:%s\n", strerror (errno));
        goto out;
    } 
    status = 0;

    free (newpath);
    newpath = NULL;
    free (oldpath);
    oldpath = NULL;

    return status;
out:
    free (newpath);
    newpath = NULL;
    free (oldpath);
    oldpath = NULL;
    return -1;
}

static int 
compress_files (const char *cwd, const char **filenames, char *result)
{
    int status = -1;

    return status;
}

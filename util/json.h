/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef __HPC_JSON_H
#define __HPC_JSON_H

#include "pool.h"
#include "log.h"

typedef struct hpc_json_s           hpc_json_t;
typedef struct hpc_json_object_s    hpc_json_object_t;
typedef struct hpc_json_array_s     hpc_json_array_t;
typedef struct hpc_json_map_s       hpc_json_map_t;
typedef struct hpc_json_item_s      hpc_json_item_t;

typedef enum {
    HPC_JSON_STRING,
    HPC_JSON_INTEGER,
    HPC_JSON_DOUBLE,
    HPC_JSON_OBJECT,
    HPC_JSON_ARRAY,
    HPC_JSON_TRUE,
    HPC_JSON_FALSE,
    HPC_JSON_NULL
} hpc_json_type;

struct hpc_json_s {
    hpc_json_type type;
    union {
        char *string;
        int intval;
        double doubleval;
        hpc_json_object_t *object;
        hpc_json_array_t *array;
        int boolean;
        void *null;
    } v;
    hpc_pool_t *pool;
};

struct hpc_json_object_s {
    hpc_json_t *self;
    hpc_json_map_t *map;
    size_t  size;
};

struct hpc_json_array_s {
    hpc_json_t *self;
    hpc_json_item_t *item;
    size_t  size;
};

struct hpc_json_map_s {
    hpc_json_object_t *obj;
    char *key;
    hpc_json_t *value;
    hpc_json_map_t *next;
};

struct hpc_json_item_s {
    hpc_json_array_t *array;
    hpc_json_t *value;
    hpc_json_item_t *next;
    size_t  index;
};

#define HPC_JSON_TYPE(json)         ((json)->type)
#define HPC_JSON_IS_STRING(json)    ((json) && HPC_JSON_TYPE(json) == HPC_JSON_STRING)
#define HPC_JSON_IS_INTEGER(json)   ((json) && HPC_JSON_TYPE(json) == HPC_JSON_INTEGER)
#define HPC_JSON_IS_DOUBLE(json)    ((json) && HPC_JSON_TYPE(json) == HPC_JSON_DOUBLE)
#define HPC_JSON_IS_OBJECT(json)    ((json) && HPC_JSON_TYPE(json) == HPC_JSON_OBJECT) 
#define HPC_JSON_IS_ARRAY(json)     ((json) && HPC_JSON_TYPE(json) == HPC_JSON_ARRAY)
#define HPC_JSON_IS_TRUE(json)      ((json) && HPC_JSON_TYPE(json) == HPC_JSON_TRUE)
#define HPC_JSON_IS_FALSE(json)     ((json) && HPC_JSON_TYPE(json) == HPC_JSON_FALSE)
#define HPC_JSON_IS_NULL(json)      ((json) && HPC_JSON_TYPE(json) == HPC_JSON_NULL)


hpc_json_t *    hpc_json_string (char *string, hpc_pool_t *pool);
hpc_json_t *    hpc_json_int (int intval, hpc_pool_t *pool);
hpc_json_t *    hpc_json_double (double doubleval, hpc_pool_t *pool);
hpc_json_t *    hpc_json_object (hpc_pool_t *pool);
hpc_json_t *    hpc_json_array (hpc_pool_t *pool);
hpc_json_t *    hpc_json_true (hpc_pool_t *pool);
hpc_json_t *    hpc_json_false (hpc_pool_t *pool);
hpc_json_t *    hpc_json_null (hpc_pool_t *pool);

char *          hpc_json_to_string (hpc_json_t *json);
void            hpc_json_write_file (hpc_json_t *json, const char *path);
hpc_json_t *    hpc_json_from_string (const char *string);

int             hpc_json_object_insert (hpc_json_t *json, const char *key, hpc_json_t *value);
int             hpc_json_object_delete (hpc_json_t *json, const char *key);
int             hpc_json_object_update (hpc_json_t *json, const char *key, hpc_json_t *value);
hpc_json_t *    hpc_json_object_get (hpc_json_t *json, const char *key);
size_t          hpc_json_object_length (hpc_json_t *json);

int             hpc_json_array_push (hpc_json_t *json, hpc_json_t *value);
hpc_json_t *    hpc_json_array_pop (hpc_json_t *json);
int             hpc_json_array_insert (hpc_json_t *json, size_t index, hpc_json_t *value);
int             hpc_json_array_update (hpc_json_t *json, size_t index, hpc_json_t *value);
int             hpc_json_array_remove (hpc_json_t *json, size_t index);
hpc_json_t *    hpc_json_array_get (hpc_json_t *json, size_t index);
size_t          hpc_json_array_length (hpc_json_t *json);

#endif

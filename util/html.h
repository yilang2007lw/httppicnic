/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef __HPC_HTML_H
#define __HPC_HTML_H

#include "log.h"
#include "pool.h"

typedef struct hpc_html_attr_s hpc_html_attr_t;
typedef struct hpc_html_node_s hpc_html_node_t;
typedef struct hpc_html_tree_s hpc_html_tree_t;

struct hpc_html_attr_s {
    char *key;
    char *value;
    hpc_html_attr_t *next;
    hpc_html_node_t *node;
};

struct hpc_html_node_s {
    char *tag;
    char *text;
    hpc_html_tree_t *tree;
    hpc_html_attr_t *attr;
    hpc_html_node_t *parent;
    hpc_html_node_t *child;
    hpc_html_node_t *next;
};

struct hpc_html_tree_s {
    hpc_html_node_t *html;
    hpc_html_node_t *head;
    hpc_html_node_t *title;
    hpc_html_node_t *body;
    hpc_pool_t *pool;
};

hpc_html_tree_t *   hpc_html_tree_new (const char *title, hpc_pool_t *pool);
void                hpc_html_tree_free (hpc_html_tree_t *tree);
void                hpc_html_tree_write_file (hpc_html_tree_t *tree, const char *path);
void                hpc_html_tree_inject_css (hpc_html_tree_t *tree, const char *css);
void                hpc_html_tree_inject_js (hpc_html_tree_t *tree, const char *js);

hpc_html_node_t *   hpc_html_node_new (const char *tag, const char *text, hpc_html_node_t *parent, hpc_html_tree_t *tree);
void                hpc_html_node_set_attribute (hpc_html_node_t *node, const char *key, const char *value);
char*               hpc_html_node_get_attribute (hpc_html_node_t *node, const char *key);
void                hpc_html_node_delete_attribute (hpc_html_node_t *node, const char *key);

void                hpc_html_node_set_text (hpc_html_node_t *node, const char *text);
void                hpc_html_node_set_id (hpc_html_node_t *node, const char *id);

void                hpc_html_node_add_klass (hpc_html_node_t *node, const char *klass);
void                hpc_html_node_remove_klass (hpc_html_node_t *node, const char *klass);
void                hpc_html_node_set_klass (hpc_html_node_t *node, const char *klass);

int                 hpc_html_node_get_depth (hpc_html_node_t *node);

void                hpc_html_node_traverse_bfs (hpc_html_node_t *node, void *cb (void *arg));
void                hpc_html_node_traverse_dfs (hpc_html_node_t *node, void *cb (void *arg));

#endif

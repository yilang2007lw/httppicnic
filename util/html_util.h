/**
 * * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef __HTML_UTIL_H
#define __HTML_UTIL_H

typedef struct _NodeAttr {
    char *key;
    char *value;
    struct _NodeAttr *next;
} NodeAttr, *NodeAttrList;

typedef struct _HtmlNode {
    char *tag;
    char *text;
    NodeAttrList attrlist;
    struct _HtmlNode *parent;
    struct _HtmlNode *child;
    struct _HtmlNode *next;
} HtmlNode, *HtmlNodeTree;

HtmlNodeTree html_node_tree_new (const char *tag, const char *text, HtmlNode *parent);

void html_node_tree_free (HtmlNodeTree tree);

void html_node_tree_set_attribute (HtmlNodeTree tree, const char *key, const char *value);

char* html_node_tree_get_attribute (HtmlNodeTree tree, const char *key);

void html_node_tree_delete_attribute (HtmlNodeTree tree, const char *key);

void html_node_tree_set_id (HtmlNodeTree tree, const char *id);

void html_node_tree_add_klass (HtmlNodeTree tree, const char *klass);

void html_node_tree_remove_klass (HtmlNodeTree tree, const char *klass);

int html_node_tree_has_klass (HtmlNodeTree tree, const char *klass);

void html_node_tree_set_klass (HtmlNodeTree tree, const char *klass);

void html_node_tree_set_text (HtmlNodeTree tree, const char *text);

void html_node_tree_inject_css (HtmlNodeTree tree, const char *css);

void html_node_tree_inject_js (HtmlNodeTree tree, const char *js);

void html_node_tree_print (HtmlNodeTree tree);

int get_node_tree_depth (HtmlNodeTree tree);

HtmlNodeTree get_node_tree_root (HtmlNodeTree tree);

HtmlNodeTree get_node_tree_parent (HtmlNodeTree tree, int depth);

HtmlNodeTree html_node_tree_find_by_tag (HtmlNodeTree tree, const char *tag);

HtmlNodeTree html_node_tree_find_by_id (HtmlNodeTree tree, const char *id);

HtmlNodeTree html_node_tree_find_by_klass (HtmlNodeTree tree, const char *klass);

void write_node_tree_to_html (HtmlNodeTree tree, const char *path);

HtmlNodeTree build_node_tree_from_html (const char *path);

#endif

/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include "json.h"

hpc_json_t *
hpc_json_string (char *string, hpc_pool_t *pool)
{
    hpc_json_t  *json = (hpc_json_t *)  hpc_pool_alloc (pool, sizeof (hpc_json_t));
    if (json == NULL) {
        hpc_warn ("%s", "alloc for json string");
        return NULL;
    }
    json->type = HPC_JSON_STRING;
    json->pool = pool;
    json->v.string = hpc_pool_dup (pool, string);

    return json;
}

hpc_json_t *       
hpc_json_int (int intval, hpc_pool_t *pool)
{
    hpc_json_t  *json = (hpc_json_t *)  hpc_pool_alloc (pool, sizeof (hpc_json_t));
    if (json == NULL) {
        hpc_warn ("%s", "alloc for json int");
        return NULL;
    }
    json->type = HPC_JSON_INTEGER;
    json->pool = pool;
    json->v.intval = intval;

    return json;
}

hpc_json_t *      
hpc_json_double (double doubleval, hpc_pool_t *pool)
{
    hpc_json_t  *json = (hpc_json_t *)  hpc_pool_alloc (pool, sizeof (hpc_json_t));
    if (json == NULL) {
        hpc_warn ("%s", "alloc for json double");
        return NULL;
    }
    json->type = HPC_JSON_DOUBLE;
    json->pool = pool;
    json->v.doubleval = doubleval;

    return json;
}

hpc_json_t * 
hpc_json_object (hpc_pool_t *pool)
{
    hpc_json_t  *json = (hpc_json_t *)  hpc_pool_alloc (pool, sizeof (hpc_json_t));
    if (json == NULL) {
        hpc_warn ("%s", "alloc for json object");
        return NULL;
    }
    json->type = HPC_JSON_OBJECT;
    json->pool = pool;

    hpc_json_object_t *obj = (hpc_json_object_t *) hpc_pool_alloc (pool, sizeof (hpc_json_object_t));
    if (obj == NULL) {
        hpc_warn ("%s", "alloc for hpc json object t");
        return NULL;
    }
    obj->self = json;
    obj->map = NULL;
    obj->size = 0;

    json->v.object = obj;

    return json;
}

hpc_json_t *  
hpc_json_array (hpc_pool_t *pool)
{
    hpc_json_t  *json = (hpc_json_t *)  hpc_pool_alloc (pool, sizeof (hpc_json_t));
    if (json == NULL) {
        hpc_warn ("%s", "alloc for json array");
        return NULL;
    }
    json->type = HPC_JSON_ARRAY;
    json->pool = pool;

    hpc_json_array_t *array = (hpc_json_array_t *) hpc_pool_alloc (pool, sizeof (hpc_json_array_t));

    if (array == NULL) {
        hpc_warn ("%s", "alloc for hpc json array t");
        return NULL;
    }
    array->self = json;
    array->item = NULL;

    json->v.array = array;

    return json;
}

hpc_json_t *       
hpc_json_true (hpc_pool_t *pool)
{
    hpc_json_t  *json = (hpc_json_t *)  hpc_pool_alloc (pool, sizeof (hpc_json_t));
    if (json == NULL) {
        hpc_warn ("%s", "alloc for json true");
        return NULL;
    }
    json->type = HPC_JSON_TRUE;
    json->pool = pool;
    json->v.boolean = 1;

    return json;
}

hpc_json_t *
hpc_json_false (hpc_pool_t *pool)
{
    hpc_json_t  *json = (hpc_json_t *)  hpc_pool_alloc (pool, sizeof (hpc_json_t));
    if (json == NULL) {
        hpc_warn ("%s", "alloc for json false");
        return NULL;
    }
    json->type = HPC_JSON_FALSE;
    json->pool = pool;
    json->v.boolean = 0;

    return json;
}

hpc_json_t * 
hpc_json_null (hpc_pool_t *pool)
{
    hpc_json_t  *json = (hpc_json_t *)  hpc_pool_alloc (pool, sizeof (hpc_json_t));
    if (json == NULL) {
        hpc_warn ("%s", "alloc for json null");
        return NULL;
    }
    json->type = HPC_JSON_FALSE;
    json->pool = pool;
    json->v.null = NULL;

    return json;
}

char *
hpc_json_to_string (hpc_json_t *json)
{
    return NULL;
}

static void
hpc_json_write_stream (hpc_json_t *json, FILE *stream) 
{
    ;
}

void
hpc_json_write_file (hpc_json_t *json, const char *path)
{
    if (json == NULL || path == NULL) {
        hpc_warn ("%s", "invalid json or path to write file");
        return;
    }

    FILE *stream = NULL;
    stream = fopen (path, "w");
    if (stream == NULL) {
        hpc_error ("%s", "write json to file:fopen");
        return;
    }
    
    hpc_json_write_stream (json, stream);
    
    if (fclose (stream) != 0) {
        hpc_error ("%s", "write json to file:fclose");
        return;
    }
}

hpc_json_t *    
hpc_json_from_string (const char *string)
{
    return NULL;
}

int   
hpc_json_object_insert (hpc_json_t *json, const char *key, hpc_json_t *value)
{
    if (!HPC_JSON_IS_OBJECT(json)) {
        hpc_warn ("%s", "not json object");
        return 0;
    }
    hpc_json_object_t *obj = json->v.object;
    if (obj == NULL) {
        hpc_warn ("%s", "obj null");
        return 0;
    }
    hpc_json_map_t *map = obj->map;
    while (map != NULL) {
        if (strcmp (map->key, key) == 0) {
            hpc_warn ("key %s already exists", key);
            return 0;
        }
        map = map->next;
    }

    map = (hpc_json_map_t *) hpc_pool_alloc (json->pool, sizeof (hpc_json_map_t));
    if (map == NULL) {
        hpc_warn ("%s", "alloc for hpc json map t"); 
        return 0;
    }
    map->key = hpc_pool_dup (json->pool, key);
    map->value = value;
    map->obj = obj;
    map->next = NULL;
    obj->size++;
    if (obj->size == 1) {
        obj->map = map;
    }

    return 1;
}

int  
hpc_json_object_delete (hpc_json_t *json, const char *key)
{
    if (!HPC_JSON_IS_OBJECT(json)) {
        hpc_warn ("%s", "not json object");
        return 0;
    }
    hpc_json_object_t *obj = json->v.object;
    if (obj == NULL) {
        hpc_warn ("%s", "obj null");
        return 0;
    }
    hpc_json_map_t *map = obj->map;
    if (map == NULL) {
        hpc_debug ("%s", "empty map");
        return 0;
    }
    if (obj->size == 1) {
        if (strcmp (map->key, key) == 0) {
            obj->map = NULL;
            return 1;
        }
        hpc_debug ("one element map don't contains %s", key);
        return 0;
    }

    hpc_json_map_t *tmp = map->next;
    while (tmp != NULL) {
        if (strcmp (tmp->key, key) == 0) {
            map->next = tmp->next;
            return 1;
        }
        map = map->next;
        tmp = tmp->next;
    }

    hpc_debug ("map don't contains %s", key);

    return 0;
}

int
hpc_json_object_update (hpc_json_t *json, const char *key, hpc_json_t *value)
{
    if (!HPC_JSON_IS_OBJECT(json)) {
        hpc_warn ("%s", "not json object");
        return 0;
    }
    hpc_json_object_t *obj = json->v.object;
    if (obj == NULL) {
        hpc_warn ("%s", "obj null");
        return 0;
    }
    hpc_json_map_t *map = obj->map;
    while (map != NULL) {
        if (strcmp (map->key, key) == 0) {
            map->value = value;
            return 1;
        }
        map = map->next;
    }

    return 0;
}

hpc_json_t *    
hpc_json_object_get (hpc_json_t *json, const char *key)
{
    if (!HPC_JSON_IS_OBJECT(json)) {
        hpc_warn ("%s", "not json object");
        return 0;
    }
    hpc_json_object_t *obj = json->v.object;
    if (obj == NULL) {
        hpc_warn ("%s", "obj null");
        return 0;
    }
    hpc_json_map_t *map = obj->map;
    while (map != NULL) {
        if (strcmp (map->key, key) == 0) {
            return map->value;
        }
        map = map->next;
    }

    return NULL;
}

size_t
hpc_json_object_length (hpc_json_t *json)
{
    if (!HPC_JSON_IS_OBJECT(json)) {
        hpc_warn ("%s", "not json object");
        return 0;
    }
    hpc_json_object_t *obj = json->v.object;
    if (obj == NULL) {
        hpc_warn ("%s", "obj null");
        return 0;
    }

    return obj->size;
}

int
hpc_json_array_push (hpc_json_t *json, hpc_json_t *value)
{
    if (!HPC_JSON_IS_ARRAY(json)) {
        hpc_warn ("%s", "not json array");
        return 0;
    }
    hpc_json_array_t *array = json->v.array;
    if (array == NULL) {
        hpc_warn ("%s", "array null");
        return 0;
    }
    hpc_json_item_t *item = array->item;
    size_t index = 0;
    while (item != NULL) {
        item = item->next;
        index++;
    }

    item = (hpc_json_item_t *) hpc_pool_alloc (json->pool, sizeof (hpc_json_item_t));
    if (item == NULL) {
        hpc_warn ("%s", "alloc for item");
        return 0;
    }
    item->array = array;
    item->index = index;
    item->value = value;
    item->next = NULL;
    array->size++;
    if (array->size == 1) {
        array->item = item;
    }

    return 1;
}

hpc_json_t *    
hpc_json_array_pop (hpc_json_t *json)
{
    if (!HPC_JSON_IS_ARRAY(json)) {
        hpc_warn ("%s", "not json array");
        return NULL;
    }
    hpc_json_array_t *array = json->v.array;
    if (array == NULL) {
        hpc_warn ("%s", "array null");
        return NULL;
    }
    hpc_json_item_t *item = array->item;
    if (item == NULL) {
        hpc_warn ("%s", "no item in array");
        return NULL;
    }
    hpc_json_item_t *tmp = item;
    while (tmp->next != NULL) {
        item = item->next;
        tmp = tmp->next;
    }
    item->next = NULL;
    array->size--;

    return tmp->value;
}

int
hpc_json_array_insert (hpc_json_t *json, size_t index, hpc_json_t *value)
{
    if (!HPC_JSON_IS_ARRAY(json)) {
        hpc_warn ("%s", "not json array");
        return 0;
    }
    hpc_json_array_t *array = json->v.array;
    if (array == NULL) {
        hpc_warn ("%s", "array null");
        return 0;
    }
    if (index > array->size - 1 || array->size == 0) {
        hpc_warn ("%s", "index exceed size");
        return 0;
    }
    hpc_json_item_t *item = array->item;
    hpc_json_item_t *tmp = item->next;
    hpc_json_item_t *new = (hpc_json_item_t *) hpc_pool_alloc (json->pool, sizeof (hpc_json_item_t));
    if (new == NULL) {
        hpc_warn ("%s", "alloc for hpc json item t");
        return 0;
    }
    new->array = array;
    new->value = value;
    new->next = NULL;
    new->index = index;
    array->size++;

    size_t i = 0;
    while (i < array->size) {
        if (item->next == NULL) {
            hpc_warn ("%s", "item next NULL");
            return 0;
        }
        if (tmp->index == index) {
            item->next = new;
            new->next = tmp;
        }
        if (tmp->index >= index) {
            tmp->index++;
        }
        item = item->next;
        tmp = tmp->next;
        i++;
    }

    return 1;
}

int
hpc_json_array_update (hpc_json_t *json, size_t index, hpc_json_t *value)
{
    if (!HPC_JSON_IS_ARRAY(json)) {
        hpc_warn ("%s", "not json array");
        return 0;
    }
    hpc_json_array_t *array = json->v.array;
    if (array == NULL) {
        hpc_warn ("%s", "array null");
        return 0;
    }
    if (index > array->size - 1 || array->size == 0) {
        hpc_warn ("%s", "index exceed size");
        return 0;
    }
    hpc_json_item_t *item = array->item;
    size_t i = 0;
    while (i < index) {
        if (item->next == NULL) {
            hpc_warn ("%s", "item next NULL");
            return 0;
        }
        item = item->next;
        i++;
    }
    item->value = value;

    return 1;
}

int
hpc_json_array_remove (hpc_json_t *json, size_t index)
{
    if (!HPC_JSON_IS_ARRAY(json)) {
        hpc_warn ("%s", "not json array");
        return 0;
    }
    hpc_json_array_t *array = json->v.array;
    if (array == NULL) {
        hpc_warn ("%s", "array null");
        return 0;
    }
    if (index > array->size - 1 || array->size == 0) {
        hpc_warn ("%s", "index exceed size");
        return 0;
    }
    hpc_json_item_t *item = array->item;
    hpc_json_item_t *tmp = item->next;
    size_t i = 0;
    while (i < array->size) {
        if (item->next == NULL) {
            hpc_warn ("%s", "item next NULL");
            return 0;
        }
        if (tmp->index == index) {
            item->next = tmp->next;
        }
        if (item->index > index) {
            item->index--;
        }
        item = item->next;
        tmp = tmp->next;
        i++;
    }

    return 1;
}

hpc_json_t *
hpc_json_array_get (hpc_json_t *json, size_t index)
{
    if (!HPC_JSON_IS_ARRAY(json)) {
        hpc_warn ("%s", "not json array");
        return NULL;
    }
    hpc_json_array_t *array = json->v.array;
    if (array == NULL) {
        hpc_warn ("%s", "array null");
        return NULL;
    }
    if (index > array->size - 1 || array->size == 0) {
        hpc_warn ("%s", "index exceed size");
        return NULL;
    }
    hpc_json_item_t *item = array->item;
    size_t i = 0;
    while (i < index) {
        if (item->next == NULL) {
            hpc_warn ("%s", "item next NULL");
            return NULL;
        }
        item = item->next;
        i++;
    }

    return item->value;
}

size_t
hpc_json_array_length (hpc_json_t *json)
{
    if (!HPC_JSON_IS_ARRAY(json)) {
        hpc_warn ("%s", "not json array");
        return 0;
    }
    hpc_json_array_t *array = json->v.array;
    if (array == NULL) {
        hpc_warn ("%s", "array null");
        return 0;
    }

    return array->size;
}

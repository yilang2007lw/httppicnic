/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef __HPC_MESSAGE_H
#define __HPC_MESSAGE_H

typedef struct hpc_request_s            hpc_request_t;
typedef struct hpc_response_s           hpc_response_t;
typedef struct hpc_request_line_s       hpc_request_line_t;  
typedef struct hpc_response_line_s      hpc_response_line_t;
typedef struct hpc_header_s             hpc_header_t;
typedef struct hpc_body_s               hpc_body_t;

typedef enum hpc_request_method {
    HPC_METHOD_GET,
    HPC_METHOD_HEAD,
    HPC_METHOD_POST,
    HPC_METHOD_PUT,
    HPC_METHOD_TRACE,
    HPC_METHOD_OPTIONS,
    HPC_METHOD_DELETE
};

struct hpc_request_s {
    hpc_request_line_t  *start;
    hpc_header_t        *header;
    hpc_body_t          *body;
    hpc_pool_t          *pool;
};

struct hpc_response_s {
    hpc_response_line_t *start;
    hpc_header_t        *header;
    hpc_body_t          *body;
    hpc_request_t       *request;
}

struct hpc_request_line_s {
    hpc_request_method  method;
    char               *url;
    char               *version;
};

struct hpc_response_line_s {
    char   *version;
    int     status;
    char   *reason;
};

struct hpc_header_s {
    char            *key;
    char            *value;
    hpc_header_t    *next;
};

struct hpc_body_s {
    void *content;
};

hpc_request_t *     hpc_request_new (const char *string, hpc_pool_t *pool);
hpc_response_t *    hpc_response_new (hpc_request_t *request);

char *              hpc_request_get_url (hpc_request_t *request);
char *              hpc_request_get_header (hpc_request_t *request, const char *key);

int                 hpc_response_set_status (hpc_response_t *response, int status, char *reason);
int                 hpc_response_add_header (hpc_response_t *response, const char *key, const char *value);
int                 hpc_response_add_buffer (hpc_response_t *reponse, const char *buffer, size_t length);
int                 hpc_response_add_file (hpc_response_t *response, const char *path);

#endif

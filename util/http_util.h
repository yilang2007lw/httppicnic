/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef __HTTP_UTIL_H
#define __HTTP_UTIL_H

char *get_server_address ();

int get_server_port ();

char *get_handle_func_for_url (const char *url);

char *get_query_value (const char *url, const char *query);

char *get_absolute_path (const char *coordinate, const char *relative);

char *get_relative_path (const char *coordinate, const char *absolute);

char *get_user_home ();

char *get_escape_html (const char *html);

#endif

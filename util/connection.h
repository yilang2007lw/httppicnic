/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef __HPC_CONNECTION_H
#define __HPC_CONNECTION_H

#include "message.h"

typedef struct hpc_connection_s     hpc_connection_t;

struct hpc_connection_s {
    int fd;
};

hpc_connection_t *  hpc_connection_new (hpc_pool_t *pool);
int                 hpc_connection_read (hpc_connection_t *connection, char *buf, size_t length);
int                 hpc_connection_write (hpc_connection_t *connection, char *buf, size_t length);
int                 hpc_connection_close (hpc_connection_t *connection);

#endif

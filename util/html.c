/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include "html.h"

static hpc_html_attr_t *    hpc_html_attr_new (const char *key, const char *value, hpc_html_node_t *node);

hpc_html_tree_t *   
hpc_html_tree_new (const char *title, hpc_pool_t *pool)
{
    hpc_html_tree_t *tree = (hpc_html_tree_t *) hpc_pool_alloc (pool, sizeof (*tree));
    if (tree == NULL) {
        hpc_warn ("%s", "alloc for hpc html tree");
        return NULL;
    }
    tree->pool = pool;

    hpc_html_node_t *html = hpc_html_node_new ("html", NULL, NULL, tree);
    if (html == NULL) {
        hpc_warn ("%s", "add root node for hpc html tree");
    }
    tree->html = html;

    hpc_html_node_t *head = hpc_html_node_new ("head", NULL, tree->html, tree);
    if (head == NULL) {
        hpc_warn ("%s", "add head node for hpc html tree");
    }
    tree->head = head;

    hpc_html_node_t *title_node = hpc_html_node_new ("title", NULL, tree->head, tree);
    if (title_node == NULL) {
        hpc_warn ("%s", "add title node for hpc html tree");
    }
    if (title != NULL) {
        hpc_html_node_set_text (title_node, title);
    }
    tree->title = title_node;

    hpc_html_node_t *body = hpc_html_node_new ("body", NULL, tree->html, tree);
    if (body == NULL) {
        hpc_warn ("%s", "add body node for hpc html tree");
    }
    tree->body = body;

    return tree;
}

void                
hpc_html_tree_free (hpc_html_tree_t *tree)
{
    hpc_pool_destroy (tree->pool);
}

static void
hpc_html_node_write_stream (hpc_html_node_t *node, FILE *stream)
{
    if (node == NULL || node->tag == NULL || stream == NULL) {
        hpc_warn ("%s", "write node tree:invalid tag");
        return;
    }

    int depth = hpc_html_node_get_depth (node);
    char *start = (char *) hpc_pool_calloc (node->tree->pool, 255);
    char *end = (char *) hpc_pool_calloc (node->tree->pool, 255);
    if (start == NULL || end == NULL) {
        hpc_error ("%s", "write node tree:calloc for start or end");
        return;
    }

    int level = depth;
    while (level > 0) {
        strncat (start, "\t", 1);
        level--;
    }
    strncat (start, "<", 1);
    strncat (start, node->tag, strlen (node->tag));

    hpc_html_attr_t *attr = node->attr;
    while (attr != NULL) {
        strncat (start, " ", 1);
        strncat (start, attr->key, strlen (attr->key));
        strncat (start, "=\"", 2); 
        strncat (start, attr->value, strlen (attr->value));
        strncat (start, "\"", 1);
        attr = attr->next;
    }
    strncat (start, ">", 1);
    //printf ("write node tree start:%s\n", start);
    fputs (start, stream);

    hpc_html_node_t *p = node->child;
    if (p == NULL) {
        if (node->text != NULL) {
            fputs (node->text, stream);
        }
        strncat (end, "</", 2);
        strncat (end, node->tag, strlen (node->tag));
        strncat (end, ">\n", 2);
        //printf ("write node tree end:%s\n", end);
        fputs (end, stream);
        fflush (stream);
    } else {
        fputs ("\n", stream);
        while (p != NULL) {
            hpc_html_node_write_stream (p, stream);
            p = p->next;
        }
        level = depth;
        while (level > 0) {
            strncat (end, "\t", 1);
            level--;
        }
        strncat (end, "</", 2);
        strncat (end, node->tag, strlen (node->tag));
        strncat (end, ">\n", 2);
        //printf ("write node tree end:%s\n", end);
        fputs (end, stream);
        fflush (stream);
    }
}

void               
hpc_html_tree_write_file (hpc_html_tree_t *tree, const char *path)
{
    if (tree == NULL || path == NULL) {
        hpc_warn ("%s", "invalid tree or path to write file");
        return;
    }

    FILE *stream = NULL;
    stream = fopen (path, "w");
    if (stream == NULL) {
        hpc_error ("%s", "write node tree to html:fopen");
        return;
    }

    hpc_html_node_write_stream (tree->html, stream);
    
    if (fclose (stream) != 0) {
        hpc_error ("%s", "write node tree to html:fclose");
        return;
    }
}

void  
hpc_html_tree_inject_css (hpc_html_tree_t *tree, const char *css)
{
    if (tree == NULL || tree->head == NULL || css == NULL) {
        hpc_warn ("%s", "invalid tree or css");
        return;
    }

    if (access (css, R_OK) != 0) {
        hpc_warn ("%s", "inject css:can't read file");
        return;
    }

    hpc_html_node_t *css_node = hpc_html_node_new ("link", NULL, tree->head, tree);
    if (css_node == NULL) {
        hpc_warn ("%s", "inject css:css node NULL");
        return;
    }

    hpc_html_node_set_attribute (css_node, "href", css);
    hpc_html_node_set_attribute (css_node, "rel", "stylesheet");
    hpc_html_node_set_attribute (css_node, "type", "text/css");
}

void
hpc_html_tree_inject_js (hpc_html_tree_t *tree, const char *js)
{
    if (tree == NULL || tree->body == NULL || js == NULL) {
        hpc_warn ("%s", "html node tree inject js");
        return;
    }

    if (access (js, R_OK) != 0) {
        hpc_warn ("%s", "inject js:can't read file");
        return;
    }

    hpc_html_node_t *js_node = hpc_html_node_new ("script", NULL, tree->body, tree);
    if (js_node == NULL) {
        hpc_warn ("%s", "inject js:js tree NULL");
        return;
    }

    hpc_html_node_set_attribute (js_node, "src", js);
    hpc_html_node_set_attribute (js_node, "type", "text/javascript");
}

static hpc_html_attr_t *    
hpc_html_attr_new (const char *key, const char *value, hpc_html_node_t *node)
{
    hpc_html_attr_t *attr = NULL;

    if (node == NULL || node->tree == NULL) {
        hpc_warn ("%s", "invalid node to crate attr"); 
        return NULL;
    }

    attr = (hpc_html_attr_t *) hpc_pool_alloc (node->tree->pool, sizeof (hpc_html_attr_t));
    if (attr == NULL) {
        hpc_warn ("%s", "alloc for hpc html attr");
        return NULL;
    }

    attr->key = (char *) hpc_pool_calloc (node->tree->pool, 255);
    if (attr->key == NULL) {
        hpc_warn ("%s", "alloc for hpc html attr key");
    }
    if (key != NULL) {
        strncpy (attr->key, key, strlen (key) + 1);
    }

    attr->value = (char *) hpc_pool_calloc (node->tree->pool, 255);
    if (attr->value == NULL) {
        hpc_warn ("%s", "alloc for hpc html attr value");
    }
    if (value != NULL) {
        strncpy (attr->value, value, strlen (value) + 1);
    }

    attr->next = NULL;
    attr->node = node;
    
    return attr;
}

hpc_html_node_t *   
hpc_html_node_new (const char *tag, const char *text, hpc_html_node_t *parent, hpc_html_tree_t *tree)
{
    hpc_html_node_t *node = NULL;

    if (tree == NULL || tag == NULL) {
        hpc_warn ("%s", "invalid tree or tag");
        return NULL;
    }

    node = (hpc_html_node_t *) hpc_pool_alloc (tree->pool, sizeof (hpc_html_node_t));
    if (node == NULL) {
        hpc_warn ("%s", "alloc for hpc html node");
        return NULL;
    }

    node->tag = (char *) hpc_pool_calloc (tree->pool, 255);
    if (node->tag == NULL) {
        hpc_warn ("%s", "alloc for hpc html node tag");
        return NULL;
    }
    strncpy (node->tag, tag, strlen (tag) + 1);

    if (text != NULL) {
        node->text = (char *) hpc_pool_calloc (tree->pool, 1023);
        if (node->text == NULL) {
            hpc_warn ("%s", "alloc for hpc html node text");
            return NULL;
        }
        strncpy (node->text, text, strlen (text) + 1);
    } else {
        node->text = NULL;
    }

    node->tree = tree;
    node->attr = NULL;
    if (parent != NULL) {
        if (parent->child == NULL) {
           parent->child = node;
        } else {
            hpc_html_node_t  *last = parent->child;
            while (last->next != NULL) {
                last = last->next;
            }
            last->next = node;
        }
    } 
    node->parent = parent;

    node->child = NULL;
    node->next = NULL;

    return node;
}

void 
hpc_html_node_set_attribute (hpc_html_node_t *node, const char *key, const char *value)
{
    if (node == NULL || key == NULL) {
        hpc_warn ("%s", "invalid node or key to set attr");
        return ;
    }
    
    if (node->attr == NULL) {
        node->attr = hpc_html_attr_new (key, value, node);
    }

    hpc_html_attr_t *attr = node->attr;
    hpc_html_attr_t *tmp = NULL;
    while (attr != NULL) {
        if (strcmp (key, attr->key) == 0) {
            strncpy (attr->value, value, strlen (value) + 1);
            return;
        }
        tmp = attr;
        attr = attr->next;
    }
    hpc_html_attr_t *new = hpc_html_attr_new (key, value, node);
    if (new == NULL) {
        hpc_warn ("%s", "new attr to node failed when set attribute");
        return;
    }
    tmp->next = new;
}

char*         
hpc_html_node_get_attribute (hpc_html_node_t *node, const char *key)
{
    if (node == NULL || key == NULL) {
        hpc_warn ("%s", "invalid node or key to get attr");
        return NULL;
    }

    if (node->attr == NULL) {
        node->attr = hpc_html_attr_new (NULL, NULL, node);
        return NULL;
    }

    hpc_html_attr_t *attr = node->attr;
    while (attr != NULL) {
        if (strcmp (key, attr->key) == 0) {
            return attr->value;
        }
        attr = attr->next;
    }

    return NULL;
}

void     
hpc_html_node_delete_attribute (hpc_html_node_t *node, const char *key)
{
    if (node == NULL || key == NULL) {
        hpc_warn ("%s", "invalid node or key to delete attr");
        return ;
    }

    hpc_html_attr_t *attr = node->attr;
    if (attr == NULL) {
        return;
    }
    if (strcmp (key, attr->key) == 0) {
       node->attr = attr->next; 
       return;
    }

    hpc_html_attr_t *tmp = attr->next;
    while (tmp != NULL) {
        if (strcmp (key, tmp->key) == 0) {
            attr->next = tmp->next;
            return;
        }
        attr = attr->next;
        tmp = tmp->next;
    }
}

void
hpc_html_node_set_text (hpc_html_node_t *node, const char *text)
{
    if (node == NULL || text == NULL) {
        hpc_warn ("%s", "invalid node or text to set node text");
        return;
    }
    if (node->text == NULL) {
        node->text = (char *) hpc_pool_calloc (node->tree->pool, 1023);
        if (node->text == NULL) {
            hpc_warn ("%s", "alloc for hpc html node text");
            return;
        }
    }
    strncpy (node->text, text, strlen (text) + 1);
}

void     
hpc_html_node_set_id (hpc_html_node_t *node, const char *id)
{
    if (node == NULL || id == NULL) {
        hpc_warn ("%s", "invalid node or id to set node id");
        return;
    }
    hpc_html_node_set_attribute (node, "id", id);
}

void 
hpc_html_node_add_klass (hpc_html_node_t *node, const char *klass)
{
    if (node == NULL || klass == NULL) {
        hpc_warn ("%s", "html node tree add class");
        return;
    }

    char *current = hpc_html_node_get_attribute (node, "class");
    char *new = hpc_pool_calloc (node->tree->pool, 255);
    if (new == NULL) {
        hpc_warn ("%s", "calloc for add klass");
        return ;
    }

    int found = 0;
    if (current != NULL) {
        strncat (new, current, strlen (current));
        char *p = NULL;
        p = strtok (current, " ");
        if (strcmp (p, klass) == 0) {
            found = 1;
        }
        while ((p = strtok (NULL, " ")) && (!found)) {
            if (strcmp (p, klass) == 0) {
                found = 1;
                break;
            }
        }
    }
    if (found != 1) {
        strncat (new, " ", 1);
        strncat (new, klass, strlen (klass));
    }

    hpc_html_node_set_attribute (node, "class", new);
}

void
hpc_html_node_remove_klass (hpc_html_node_t *node, const char *klass)
{
    if (node == NULL || klass == NULL) {
        hpc_warn ("%s", "invalid node or klass to remove");
        return;
    }

    char *current = hpc_html_node_get_attribute (node, "class");
    if (current != NULL) {
        char *new = (char *) hpc_pool_calloc (node->tree->pool, 255);
        if (new == NULL) {
            hpc_warn ("%s", "calloc for remove klass");
            return ;
        }
        char *p = NULL;
        p = strtok (current, " ");
        if (strcmp (p, klass) != 0) {
            strncat (new, p, strlen (p));
            strncat (new, " ", 1);
        }
        while ((p = strtok (NULL, " "))) {
            if (strcmp (p, klass) != 0) {
                strncat (new, p, strlen (p));
                strncat (new, " ", 1);
            }
        }
        if (strlen (new) == 0) {
            hpc_html_node_set_attribute (node, "class", "");
        } else {
            hpc_html_node_set_attribute (node, "class", new);
        }
    }
}

void    
hpc_html_node_set_klass (hpc_html_node_t *node, const char *klass)
{
    if (node == NULL || klass == NULL) {
        hpc_warn ("%s", "invalid node or klass to set node class");
        return;
    }
    hpc_html_node_set_attribute (node, "class", klass);
}

int                 
hpc_html_node_get_depth (hpc_html_node_t *node)
{
    int depth = -1;

    if (node == NULL) {
        hpc_warn ("%s", "invalid node for get depth"); 
        return depth;
    }
    if (node->tree == NULL) {
        hpc_warn ("%s", "node tree NULL for get depth"); 
        return depth;
    }

    hpc_html_node_t *p = node;
    while (p != NULL) {
        depth++;
        p = p->parent;
    }

    return depth;
} 
void
hpc_html_node_traverse_bfs (hpc_html_node_t *node, void *cb (void *arg))
{
    if (node != NULL) {
        cb (node);
        while (node->next != NULL) {
            hpc_html_node_traverse_bfs (node->next, cb);
        }
        if (node->child != NULL) {
            hpc_html_node_traverse_bfs (node->child, cb);
        }
    } else {
        hpc_warn ("%s", "traverse bfs node NULL");
    }
}

void
hpc_html_node_traverse_dfs (hpc_html_node_t *node, void *cb (void *arg))
{
    if (node != NULL) {
        cb (node);
        while (node->child != NULL) {
            hpc_html_node_traverse_dfs (node->child, cb);
        }
        if (node->next != NULL) {
            hpc_html_node_traverse_dfs (node->next, cb);
        }
    } else {
        hpc_warn ("%s", "traverse dfs node NULL");
    }
}

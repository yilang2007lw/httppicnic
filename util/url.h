/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef __HPC_URL_H
#define __HPC_URL_H

#include "pool.h"
#include "log.h"

typedef struct hpc_url_s            hpc_url_t;
typedef struct hpc_url_path_s       hpc_url_path_t;
typedef struct hpc_url_param_s      hpc_url_param_t;
typedef struct hpc_url_query_s      hpc_url_query_t;

struct hpc_url_s {
    char *scheme;
    char *user;
    char *password;
    char *host;
    int port;
    hpc_url_path_t *path;
    hpc_url_query_t *query;
    char *frag;
    hpc_pool_t *pool;
};

struct hpc_url_path_s {
    hpc_url_t *self;
    char *seg;
    hpc_url_param_t *param;
    hpc_url_path_t *next;
}

struct hpc_url_param_s {
    hpc_url_path_t *self;
    char *name;
    char *value;
    hpc_url_param_t *next;
}

struct hpc_url_query_s {
    hpc_url_t *self;
    char *key;
    char *value;
    hpc_url_query_t *next;
}

hpc_url_t *     hpc_url_new (const char *url, hpc_pool_t *pool);

char *          hpc_url_to_string (hpc_url_t *url);

char *          hpc_url_get_path (hpc_url_t *url);

char *          hpc_url_escape (const char *url);

char *          hpc_url_unescape (const char *url);

int             hpc_url_valid (hpc_url_t *url);

char *          hpc_url_auto_expand (hpc_url_t *url);

char *          hpc_url_get_relative_url (hpc_url_t *url, const char *target);

char *          hpc_url_get_absolute_url (hpc_url_t *url, const char *target);

#endif

/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pool.h"

static void *hpc_pool_alloc_block (hpc_pool_t *pool, size_t size);
static void *hpc_pool_alloc_large (hpc_pool_t *pool, size_t size);

void * 
hpc_memalign (size_t alignment, size_t size)
{
    void *p;
    int err;

    err = posix_memalign (&p, alignment, size);
    if (err) {
        fprintf (stderr, "hpc memalign err\n");
        p = NULL;
    }

    return p;
}

hpc_pool_t *
hpc_pool_create (size_t size)
{
    hpc_pool_t *p = NULL;
    p = (hpc_pool_t *) hpc_memalign (HPC_ALIGNMENT, size);
    if (p == NULL) {
        return NULL;
    }

    p->d.last = (u_char *)p + sizeof(hpc_pool_t);
    p->d.end = (u_char *)p + size;
    p->d.next = NULL;

    size = size - sizeof(hpc_pool_t);
    p->max = (size < HPC_POOL_MAX_ALLOC) ? size : HPC_POOL_MAX_ALLOC;

    p->current = p;
    p->large = NULL;

    return p;
}

void 
hpc_pool_destroy (hpc_pool_t *pool)
{
    hpc_pool_t *p,*n;
    hpc_pool_large_t *l;

    for (l = pool->large; l; l = l->next) {
        if (l->alloc) {
            hpc_free (l->alloc);
        }
    }

    for (p = pool, n = pool->d.next;/*void*/; p = n, n = n->d.next) {
        hpc_free (p);
        if (n == NULL) {
            break;
        }
    }
}

void 
hpc_pool_reset (hpc_pool_t *pool)
{
    hpc_pool_t *p;
    hpc_pool_large_t *l;

    for (l = pool->large; l ; l = l->next) {
        if (l->alloc) {
            hpc_free (l->alloc);
        }
    }
    pool->large = NULL;

    for (p = pool; p; p = p->d.next) {
        p->d.last = (u_char *)p + sizeof(hpc_pool_t);
    }
}

void *
hpc_pool_alloc (hpc_pool_t *pool, size_t size)
{
    u_char *m;
    hpc_pool_t *p;

    if (size <= pool->max) {
        p = pool->current;
        do {
            m = hpc_align_ptr (p->d.last, HPC_ALIGNMENT); 
            if ((size_t) (p->d.end - m) > size) {
                p->d.last = m + size;
                return m;
            }
            p = p->d.next;
        } while (p);

        return hpc_pool_alloc_block (pool, size);
    }
    return hpc_pool_alloc_large (pool, size);
}

void *
hpc_pool_nalloc (hpc_pool_t *pool, size_t size)
{
    u_char *m;
    hpc_pool_t *p;

    if (size <= pool->max) {
        p = pool->current;
        do {
            m = p->d.last;
            if ((size_t) (p->d.end - m) > size) {
                p->d.last = m + size;
                return m;
            }
            p = p->d.next;
        } while (p);

        return hpc_pool_alloc_block (pool, size);
    }
    return hpc_pool_alloc_large (pool, size);

}

static void *
hpc_pool_alloc_block (hpc_pool_t *pool, size_t size)
{
    u_char *m;
    size_t psize;
    hpc_pool_t *p, *new;

    psize = (size_t) (pool->d.end - (u_char *) pool);

    m = hpc_memalign (HPC_ALIGNMENT, psize);
    if (m == NULL) {
        return NULL;
    }

    new = (hpc_pool_t *) m;
    new->d.end = m + psize;
    new->d.next = NULL;

    m += sizeof(hpc_pool_data_t);
    m = hpc_align_ptr (m, HPC_ALIGNMENT);
    new->d.last = m + size;

    p = pool->current;
    while (p->d.next) {
        p = p->d.next;
    }

    p->d.next = new;
    pool->current = new;

    return m;
}

static void *
hpc_pool_alloc_large (hpc_pool_t *pool, size_t size)
{
    void *p;
    hpc_pool_large_t *large;

    p = hpc_alloc (size);
    if (p == NULL) {
        return NULL;
    }
    
    for (large = pool->large; large; large = large->next) {
        if (large->alloc == NULL) {
            large->alloc = p;
            return p;
        }
    }

    large = hpc_pool_alloc (pool, sizeof (hpc_pool_large_t));
    if (large == NULL) {
        hpc_free (p);
        return NULL;
    }
    large->alloc = p;
    large->next = pool->large;
    pool->large = large;

    return p;
}

void *
hpc_pool_calloc (hpc_pool_t *pool, size_t size)
{
    void *p;
    p = hpc_pool_alloc (pool, size);
    if (p) {
        hpc_memzero(p, size);
    }
    return p;
}

void 
hpc_pool_free (hpc_pool_t *pool, void *p)
{
    hpc_pool_large_t *l;

    for (l = pool->large; l; l = l->next) {
        if (p == l->alloc) {
            hpc_free (l->alloc);
            l->alloc = NULL;
        }
    }
}

char *
hpc_pool_dup (hpc_pool_t *pool, const char *string)
{
    char *result = NULL;
    if (pool == NULL || string == NULL) {
        fprintf (stderr, "pool or string null for hpc pool dup");
        return result;
    }
    
    result = (char *) hpc_pool_calloc (pool, sizeof (string));
    if (result == NULL) {
        fprintf (stderr, "calloc failed for hpc pool dup");
        return result;
    }
    strncpy (result, string, strlen (string) + 1);

    return result;
}

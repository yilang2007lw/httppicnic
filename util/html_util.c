/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include "html_util.h"

static NodeAttr* node_attr_new (const char *key, const char *value);
static void node_attr_free (NodeAttr *attr);
static void node_attr_list_update_attr (NodeAttrList attrlist, const char *key, const char *value);
static char * node_attr_list_get_attr (NodeAttrList attrlist, const char *key);
static void node_attr_list_remove_attr (NodeAttrList attrlist, const char *key);

static HtmlNode* html_node_new (const char *tag, const char *text);
static void html_node_free (HtmlNode *node);

static NodeAttr* 
node_attr_new (const char *key, const char *value)
{
    NodeAttr *attr = NULL;
    attr = (NodeAttr *) malloc (sizeof (NodeAttr));
    if (attr == NULL) {
        perror ("node_attr_new");
        return NULL;
    }
    attr->key =  (char *) malloc (256);
    if (attr->key == NULL) {
        perror ("node attr new:key");
        free (attr);
        return NULL;
    }
    memset (attr->key, 0, 256);
    if (key != NULL) {
        strncpy (attr->key, key, strlen (key) + 1);
    }

    attr->value = (char *) malloc (256);
    if (attr->value == NULL) {
        perror ("node_attr_new:value");
        free (attr->key);
        attr->key = NULL;
        free (attr);
        attr = NULL;
        return NULL;
    }
    memset (attr->value, 0, 256);
    if (value != NULL) {
        strncpy (attr->value, value, strlen (value) + 1);
    }

    attr->next = NULL;

    return attr;
}

static void
node_attr_free (NodeAttr *attr)
{
    if (attr == NULL) {
        return ;
    }
    if (attr->key != NULL) {
        free (attr->key);
        attr->key = NULL;
    }
    if (attr->value != NULL) {
        free (attr->value);
        attr->value = NULL;
    }
    attr->next = NULL;

    free (attr);
    attr = NULL;
}

static void
node_attr_list_update_attr (NodeAttrList attrlist, const char *key, const char *value)
{
    int found = 0;

    if (attrlist == NULL || key == NULL) {
        perror ("node attr list update attr");
    }

    NodeAttr *p = attrlist;
    NodeAttr *q = attrlist->next;
    while (q != NULL) {
        if (strcmp (key, q->key) == 0) {
            found = 1;
            break;
        }
        p = p->next;
        q = q->next;
    }

    if (found) {
        if (value != NULL) {
            strncpy (q->value, value, 256);
        }

    } else {
        NodeAttr *n = node_attr_new (key, value);
        if (n == NULL) {
            perror ("node attr new");
            return ;
        }
        p->next = n;
    }
}

static char *
node_attr_list_get_attr (NodeAttrList attrlist, const char *key)
{
    char *value = NULL;

    if (attrlist == NULL || key == NULL) {
        perror ("node attr list get attr");
        return NULL;
    }

    NodeAttr *p = attrlist;
    NodeAttr *q = attrlist->next;
    while (q != NULL) {
        if (strcmp (key, q->key) == 0) {
            value = q->value;
            break;
        }
        p = p->next;
        q = q->next;
    }
    return value;
}

static void
node_attr_list_remove_attr (NodeAttrList attrlist, const char *key)
{
    int found = 0;

    if (attrlist == NULL || key == NULL) {
        perror ("node attr list remove attr");
        return ;
    }

    NodeAttr *p = attrlist;
    NodeAttr *q = attrlist->next;
    while (q != NULL) {
        if (strcmp (key, q->key) == 0) {
            found = 1;
            break;
        }
        p = p->next;
        q = q->next;
    }

    if (found) {
        p->next = q->next;
        node_attr_free (q);
    }
}

static HtmlNode* 
html_node_new (const char *tag, const char *text)
{
    HtmlNode *node = NULL;

    if (tag == NULL) {
        perror ("html node new:must specify tag");
        return NULL;
    }

    node = (HtmlNode *) malloc (sizeof (HtmlNode));
    if (node == NULL) {
        perror ("malloc");
        return NULL;
    }
    node->tag = (char *) malloc (256);
    if (node->tag == NULL) {
        perror ("malloc");
        free (node);
        node = NULL;
        return NULL;
    }
    memset (node->tag, 0, 256);
    if (tag != NULL) {
        strncpy (node->tag, tag, strlen (tag) + 1);
    }

    node->text = (char *) malloc (256);
    if (node->text == NULL) {
        perror ("malloc");
        free (node->tag);
        node->tag = NULL;
        free (node);
        node = NULL;
        return NULL;
    }
    memset (node->text, 0, 256);
    if (text != NULL) {
        strncpy (node->text, text, strlen (text) + 1);
    }

    node->attrlist = node_attr_new (NULL, NULL);
    node->parent = NULL;
    node->child = NULL;
    node->next = NULL;

    return node;
}

static void
html_node_free (HtmlNode *node)
{
    if (node == NULL) {
        return ;
    }
    if (node->tag != NULL) {
        free (node->tag);
        node->tag = NULL;
    }
    if (node->text != NULL) {
        free (node->text);
        node->text = NULL;
    }
    if (node->attrlist != NULL) {
        NodeAttr *p = node->attrlist;
        while (p != NULL) {
            NodeAttr *q = p;
            p = p->next;
            node_attr_free (q);
        }
        node->attrlist = NULL;
    }
    node->parent = NULL;
    node->child = NULL;
    node->next = NULL;
    free (node);
    node = NULL;
}

HtmlNodeTree 
html_node_tree_new (const char *tag, const char *text, HtmlNode *parent)
{
    HtmlNodeTree tree = NULL;

    tree = html_node_new (tag, text);
    if (tree == NULL) {
        perror ("html_node_new");
        return NULL;
    }

    if (parent != NULL) {
        if (parent->child == NULL) {
           parent->child = tree; 
        } else {
            HtmlNode *last = parent->child;
            while (last->next != NULL) {
                last = last->next;
            }
            last->next = tree;
        }
        tree->parent = parent;
    }

    return tree;
}

void 
html_node_tree_free (HtmlNodeTree tree)
{
    if (tree == NULL) {
        return;
    }

    HtmlNode *child = tree->child;
    while (child != NULL) {
        HtmlNode *q = child;
        child = child->next;
        html_node_tree_free (q);
    }

    HtmlNode *parent = tree->parent;
    HtmlNode *next = tree->next;
    if (parent != NULL) {
        HtmlNode *p = parent->child;
        if (p == tree) {
            parent->child = p->next;
        } else {
            while (p->next != tree) {
                p = p->next;
            }
            p->next = tree->next;
        }
    } else {
        if (next != NULL) {
            fprintf (stderr, "when parent is NULL, should not has next");
            fprintf (stderr, "cur tag:%s\n, next tag:%s\n", tree->tag, next->tag); 
        }
    }

    html_node_free (tree);
}

void 
html_node_tree_set_attribute (HtmlNodeTree tree, const char *key, const char *value)
{
    if (tree == NULL || tree->attrlist == NULL || key == NULL) {
        perror ("html node tree set attribute");
        return ;
    }

    node_attr_list_update_attr (tree->attrlist, key, value);
}

char* 
html_node_tree_get_attribute (HtmlNodeTree tree, const char *key)
{
    if (tree == NULL || tree->attrlist == NULL || key == NULL) {
        perror ("html node tree get attribute");
        return NULL;
    }

    return node_attr_list_get_attr (tree->attrlist, key);
}

void 
html_node_tree_delete_attribute (HtmlNodeTree tree, const char *key)
{
    if (tree == NULL || tree->attrlist == NULL || key == NULL) {
        perror ("html node tree delete attribute");
        return ;
    }

    node_attr_list_remove_attr (tree->attrlist, key);
}

void 
html_node_tree_set_id (HtmlNodeTree tree, const char *id)
{
    if (tree == NULL || tree->attrlist == NULL || id == NULL) {
        perror ("html node set id:tree or id is NULL");
        return ;
    }

    node_attr_list_update_attr (tree->attrlist, "id", id);
}

void 
html_node_tree_add_klass (HtmlNodeTree tree, const char *klass)
{
    if (tree == NULL || klass == NULL) {
        perror ("html node tree add class");
        return;
    }

    char *current = node_attr_list_get_attr (tree->attrlist, "class");
    char *new = (char *) malloc (256);
    if (new == NULL) {
        perror ("malloc");
        return ;
    }
    memset (new, 0, 256);
    int found = 0;

    if (current != NULL) {
        strncat (new, current, strlen (current));
        char *p = NULL;
        p = strtok (current, " ");
        if (strcmp (p, klass) == 0) {
            found = 1;
        }
        while ((p = strtok (NULL, " ")) && (!found)) {
            if (strcmp (p, klass) == 0) {
                found = 1;
                break;
            }
        }
    }
    if (found != 1) {
        strncat (new, " ", 1);
        strncat (new, klass, strlen (klass));
    }

    node_attr_list_update_attr (tree->attrlist, "class", new);
    free (new);
    new = NULL;
}

void 
html_node_tree_remove_klass (HtmlNodeTree tree, const char *klass)
{
    if (tree == NULL || klass == NULL) {
        perror ("html node tree add class");
        return;
    }

    char *current = node_attr_list_get_attr (tree->attrlist, "class");
    if (current != NULL) {
        char *p = NULL;
        char *new = (char *) malloc (256);
        if (new == NULL) {
            perror ("malloc");
            return ;
        }
        memset (new, 0, 256);
        p = strtok (current, " ");
        if (strcmp (p, klass) != 0) {
            strncat (new, p, strlen (p));
            strncat (new, " ", 1);
        }
        while ((p = strtok (NULL, " "))) {
            if (strcmp (p, klass) != 0) {
                strncat (new, p, strlen (p));
                strncat (new, " ", 1);
            }
        }
        if (strlen (new) == 0) {
            node_attr_list_remove_attr (tree->attrlist, "class");
        } else {
            node_attr_list_update_attr (tree->attrlist, "class", new);
        }
        free (new);
        new = NULL;
    }
}

int 
html_node_tree_has_klass (HtmlNodeTree tree, const char *klass)
{
    int found = 0;

    return found;
}

void 
html_node_tree_set_klass (HtmlNodeTree tree, const char *klass)
{
    if (tree == NULL || klass == NULL) {
        perror ("html node set klass:tree or klass is NULL");
        return ;
    }

    node_attr_list_update_attr (tree->attrlist, "class", klass);
}

void 
html_node_tree_set_text (HtmlNodeTree tree, const char *text)
{
    if (tree == NULL || text == NULL) {
        perror ("html node set text:tree or text is NULL");
        return ;
    }
    strncpy (tree->text, text, 256);
}

void 
html_node_tree_inject_css (HtmlNodeTree tree, const char *css)
{
    if (tree == NULL || css == NULL) {
        perror ("html node tree inject css");
        return ;
    }

    if (access (css, R_OK) != 0) {
        perror ("inject css:can't read file");
        return ;
    }

    HtmlNodeTree root = get_node_tree_root (tree);
    if (root == NULL) {
        perror ("inject css:root is NULL");
        return ;
    }
    HtmlNodeTree parent = root->child;
    //HtmlNodeTree parent = html_node_tree_find_by_tag (root, "head");
    if (parent == NULL) {
        perror ("inject css:head must ready");
        return ;
    }

    HtmlNodeTree css_tree = html_node_tree_new ("link", NULL, parent);
    if (css_tree == NULL) {
        perror ("inject css:css tree NULL");
        return ;
    }

    html_node_tree_set_attribute (css_tree, "href", css);
    html_node_tree_set_attribute (css_tree, "rel", "stylesheet");
    html_node_tree_set_attribute (css_tree, "type", "text/css");
}

void 
html_node_tree_inject_js (HtmlNodeTree tree, const char *js)
{
    if (tree == NULL || js == NULL) {
        perror ("html node tree inject js");
        return ;
    }

    if (access (js, R_OK) != 0) {
        perror ("inject js:can't read file");
        return ;
    }

    HtmlNodeTree root = get_node_tree_root (tree);
    if (root == NULL) {
        perror ("inject js:root is NULL");
        return ;
    }

    assert (root->child != NULL);
    HtmlNodeTree parent = root->child->next;
    //HtmlNodeTree parent = html_node_tree_find_by_tag (root, "body");
    if (parent == NULL) {
        perror ("inject js:body must ready");
        return ;
    }

    HtmlNodeTree js_tree = html_node_tree_new ("script", NULL, parent);
    if (js_tree == NULL) {
        perror ("inject js:js tree NULL");
        return ;
    }

    html_node_tree_set_attribute (js_tree, "src", js);
    html_node_tree_set_attribute (js_tree, "type", "text/javascript");
}

void 
html_node_tree_print (HtmlNodeTree tree)
{
    if (tree == NULL) {
        return ;
    }
    printf ("tree tag:%s\n", tree->tag);
    printf ("\ttree text:%s\n", tree->text);

    HtmlNode *child = tree->child;
    while (child != NULL) {
        printf ("print subtree for %s\n", child->parent->tag);
        html_node_tree_print (child);
        child = child->next;
    }
    //printf ("print next for tree:%s\n", tree->tag);
    //html_node_tree_print (tree->next);
}

int 
get_node_tree_depth (HtmlNodeTree tree)
{
    int depth = -1;

    if (tree == NULL) {
        return depth;
    }

    HtmlNodeTree p = tree;
    while (p != NULL) {
        depth++;
        p = p->parent;
    }

    return depth;
}

HtmlNodeTree 
get_node_tree_root (HtmlNodeTree tree)
{
    HtmlNodeTree root = NULL;
    if (tree == NULL) {
        perror ("get node tree root");
        return NULL;
    }
    root = tree;
    while (root->parent != NULL) {
        root = root->parent;
    }

    return root;
}

HtmlNodeTree
get_node_tree_parent (HtmlNodeTree tree, int depth)
{
    HtmlNodeTree parent = NULL;
    if (tree == NULL) {
        perror ("get node tree parent");
        return NULL;
    }

    int i = 0;
    HtmlNodeTree tmp = tree;
    while (i < depth && tmp != NULL) {
        parent = tmp->parent;
        tmp = parent;
    }

    return parent;
}

static int
cmp_tag (HtmlNodeTree tree, const char *tag)
{
    int equal = 0;
    if (tree != NULL && tag != NULL && (strcmp (tree->tag, tag) == 0)){
        equal = 1;
    }
    return equal;
}

static int
cmp_id (HtmlNodeTree tree, const char *id)
{
    int equal = 0;
    if (tree != NULL && id != NULL) {
        char *tid = html_node_tree_get_attribute (tree, "id");
        if (tid != NULL && (strcmp (tid, id) == 0)) {
            equal = 1;
        }
    }
    return equal;
}

static int
cmp_klass (HtmlNodeTree tree, const char *klass)
{
    return html_node_tree_has_klass (tree, klass);
}

static HtmlNodeTree
search_bfs (HtmlNodeTree start, int (*cmpfunc)(HtmlNodeTree, const char *), const char *txt)
{
    if (start != NULL) {
        if (cmpfunc (start, txt)) {
            return start;
        } else {
            while (start->next != NULL) {
                search_bfs (start->next, cmpfunc, txt);
            }
            if (start->child != NULL) {
                search_bfs (start->child, cmpfunc, txt);
            } 
        }
    } else {
        fprintf (stderr, "search bfs:start NULL\n");
    }
    
    return NULL;
}

static HtmlNodeTree
search_dfs (HtmlNodeTree start, int (*cmpfunc)(HtmlNodeTree, const char *), const char *txt)
{
    if (start != NULL) {
        if (cmpfunc (start, txt)) {
            return start;
        } else {
            while (start->child != NULL) {
                search_dfs (start->child, cmpfunc, txt);
            }
            if (start->next != NULL) {
                search_dfs (start->next, cmpfunc, txt);
            }
        }
    } else {
        fprintf (stderr, "search dfs:start NULL\n");
    }
    
    return NULL;
}

HtmlNodeTree 
html_node_tree_find_by_tag (HtmlNodeTree tree, const char *tag)
{
    if (tree == NULL) {
        perror ("find by tag:tree is NULL\n");
        return NULL;
    }

    return search_bfs (tree, cmp_tag, tag);
}

HtmlNodeTree 
html_node_tree_find_by_id (HtmlNodeTree tree, const char *id)
{
    if (tree == NULL) {
        perror ("find by tag:tree is NULL\n");
        return NULL;
    }

    return search_dfs (tree, cmp_id, id);
}

HtmlNodeTree 
html_node_tree_find_by_klass (HtmlNodeTree tree, const char *klass)
{
    if (tree == NULL) {
        perror ("find by tag:tree is NULL\n");
        return NULL;
    }

    return search_dfs (tree, cmp_klass, klass);
}

static void 
write_node_tree (HtmlNodeTree tree, FILE *stream)
{
    int depth, level;
    char *start;
    char *end;

    if (tree == NULL || tree->tag == NULL) {
        perror ("write node tree:invalid tag");
        return;
    }

    depth = get_node_tree_depth (tree);
    start = (char *) malloc (256);
    end = (char *) malloc (256);
    if (start == NULL || end == NULL) {
        perror ("write node tree:malloc");
        return;
    }
    memset (start, 0, 256);
    memset (end, 0, 256);

    level = depth;
    while (level > 0) {
        strncat (start, "\t", 1);
        level--;
    }
    strncat (start, "<", 1);
    strncat (start, tree->tag, strlen (tree->tag));

    NodeAttr *attr = tree->attrlist->next;
    while (attr != NULL) {
        strncat (start, " ", 1);
        strncat (start, attr->key, strlen (attr->key));
        strncat (start, "=\"", 2); 
        strncat (start, attr->value, strlen (attr->value));
        strncat (start, "\"", 1);
        attr = attr->next;
    }

    strncat (start, ">", 1);
    //printf ("write node tree start:%s\n", start);
    fputs (start, stream);
    free (start);
    start = NULL;

    HtmlNodeTree p = tree->child;
    if (p == NULL) {
        if (tree->text != NULL) {
            fputs (tree->text, stream);
        }
        strncat (end, "</", 2);
        strncat (end, tree->tag, strlen (tree->tag));
        strncat (end, ">\n", 2);
        //printf ("write node tree end:%s\n", end);
        fputs (end, stream);
        fflush (stream);
        free (end);
        end = NULL;

    } else {
        fputs ("\n", stream);
        while (p != NULL) {
            write_node_tree (p, stream);
            p = p->next;
        }
        level = depth;
        while (level > 0) {
            strncat (end, "\t", 1);
            level--;
        }
        strncat (end, "</", 2);
        strncat (end, tree->tag, strlen (tree->tag));
        strncat (end, ">\n", 2);
        //printf ("write node tree end:%s\n", end);
        fputs (end, stream);
        fflush (stream);
        free (end);
        end = NULL;
    }
}

void 
write_node_tree_to_html (HtmlNodeTree tree, const char *path)
{
    FILE *stream = NULL;
    stream = fopen (path, "w");
    if (stream == NULL) {
        perror ("write node tree to html:fopen");
        return;
    }

    write_node_tree (tree, stream);
    
    if (fclose (stream) != 0) {
        perror ("write node tree to html:fclose");
        return;
    }
}

HtmlNodeTree 
build_node_tree_from_html (const char *path)
{
    HtmlNodeTree tree = NULL;

    printf ("path:%s\n", path);

    return tree;
}

/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>
#include <yajl/yajl_tree.h>
#include <yajl/yajl_parse.h>
#include "http_util.h"

#define SERVER_CONFIG_PATH RESOURCE_DIR"server.conf"
#define MAX_TEXT_SIZE   65536

static yajl_val 
get_yajl_value (const char *cfg)
{
    yajl_val ret = NULL;

    FILE *config_file = NULL;
    char content[65536];
    char errbuf[1024];
    size_t rd;

    config_file = fopen (cfg, "r+");
    if (config_file == NULL) {
        fprintf (stderr, "open config file:%s\n", strerror (errno));
        goto out;
    }

    rd = fread ((void *) content, 1, sizeof(content) -1, config_file);
    if (rd == 0 && !feof(config_file)) {
        fprintf (stderr, "error encountered on file read\n");
        goto out;
    } else if (rd >= sizeof (content) -1 ) {
        fprintf (stderr, "config file too big");
        goto out;
    }

    ret = yajl_tree_parse (content, errbuf, sizeof(errbuf));
    if (strlen (errbuf)) {
        fprintf(stderr, "errbuf:%s\n", errbuf);
        goto out;
    }

    fclose (config_file);
    
    return ret;
out:
    if (config_file != NULL) {
        fclose (config_file);
    }
    return NULL;
}

char *
get_server_address ()
{
    char *address = NULL;

    yajl_val node = get_yajl_value (SERVER_CONFIG_PATH);
    const char * path[] = { "Server", "ip", (const char *) 0 };
    yajl_val val = yajl_tree_get (node, path, yajl_t_string);
    if (val != NULL) {
        address = strdup (YAJL_GET_STRING (val));
    } else {
        address = strdup ("0.0.0.0");
    }
    yajl_tree_free (node);

    return address;
}

int get_server_port ()
{
    int port = 0;

    yajl_val node = get_yajl_value (SERVER_CONFIG_PATH);
    const char * path[] = { "Server", "port", (const char *) 0 };
    yajl_val val = yajl_tree_get (node, path, yajl_t_number);
    if (val) {
        port = YAJL_GET_INTEGER (val);
    } 
    yajl_tree_free (node);

    return port;
}

char *
get_handle_func_for_url (const char *url)
{
    char *func = NULL;
    if (url == NULL) {
        fprintf (stderr, "url is NULL");
        return NULL;
    }
    if (strncmp ("/index", url, 6) == 0) {
        func = strdup ("index");
    } else if (strncmp ("/filemanager", url, 12) == 0) {
        func = strdup ("filemanager");
    } else if (strncmp ("/static/", url, 8) == 0) {
        func = strdup ("static");
    } else if (strncmp ("/resource?", url, 10) == 0) {
        func = strdup ("resource");
    } else if (strncmp ("/textfile?", url,10) == 0) {
        func = strdup ("textfile");
    } else if (strncmp ("/imagefile?", url, 11) == 0) {
        func = strdup ("imagefile");
    } else if (strncmp ("/videofile?", url, 11) == 0) {
        func = strdup ("videofile");
    } else {
        func = strdup ("defalut");
    }

    return func;
}

char *
get_query_value (const char *url, const char *query)
{
    char *result = NULL;
    char *querysub = (char *) malloc (256);
    if (querysub == NULL) {
        perror ("malloc");
        return NULL;
    }
    memset (querysub, 0, 256);

    sprintf (querysub, "?%s=", query);
    char *p = strstr (url, querysub);
    if (p == NULL) {
        printf ("get query value:%s not found for %s\n", query, url);
        free (querysub);
        querysub = NULL;
        return NULL;
    }
    char *q = p + strlen (querysub);
    while ((*q != '?') && (*q != '#') && (*q != '\0')) {
        q++;
    }
    result = strndup (p + strlen (querysub) , q - (p + strlen (querysub)));

    free (querysub);
    querysub = NULL;
   
    return result;
}

char *
get_absolute_path (const char *coordinate, const char *relative)
{
    char *path = NULL;

    path = (char *) malloc (256);
    if (path == NULL) {
        perror ("malloc");
        return NULL;
    }
    memset (path, 0, 256);

    return path;
}

char *
get_relative_path (const char *coordinate, const char *absolute)
{
    char *path = NULL;

    path = (char *) malloc (256);
    if (path == NULL) {
        perror ("malloc");
        return NULL;
    }
    memset (path, 0, 256);

    return path;
}

char *
get_user_home ()
{
    char *home = NULL;
    struct passwd *pwd;
    uid_t uid;

    //yajl_val node = get_yajl_value (SERVER_CONFIG_PATH);
    //const char * path[] = { "FileManager", "root", (const char *) 0 };
    //yajl_val val = yajl_tree_get (node, path, yajl_t_string);
    //if (val != NULL) {
    //    home = strdup (YAJL_GET_STRING (val));
    //} 
    //yajl_tree_free (node);

    if (home == NULL) {
        uid = getuid ();
        pwd = getpwuid (uid);
        if (pwd == NULL) {
            printf ("getpwuid:%s\n", strerror (errno));
            return NULL;
        }
        home = strdup (pwd->pw_dir);
    }

    return home;
}

static char *
escape_tags (const char *original)
{
    if (original == NULL) {
        printf ("escapge tags:original is NULL\n");
        return NULL;
    }

    char *result = (char *) malloc (MAX_TEXT_SIZE);
    if (result == NULL) {
        printf ("escapge tags:malloc\n");
        return NULL;
    }
    memset (result, 0, MAX_TEXT_SIZE);
    FILE *stream = fopen (original, "r");
    if (stream == NULL) {
        printf ("escape tags:fopen\n");
        free (result);
        result = NULL;
        return NULL;
    }
    int c;
    while ((c = fgetc (stream)) != EOF) {
        int found = 1;
        switch (c) {
            case 60: strcat (result, "&lt;"); break;
            case 62: strcat (result, "&gt;"); break;
            case 34: strcat (result, "&quot;"); break;
            case '\n': strcat (result, "<br/>"); break;
            case '\r': break;
            default: found = 0; break;
        }
        if (!found) {
            sprintf (result, "%s%d", result, c);
        }
    }
    return result;
}

static char *
escape_special (char *original)
{
    if (original == NULL) {
        printf ("escapge tags:original is NULL\n");
        return NULL;
    }

    char *result = (char *) malloc (MAX_TEXT_SIZE);
    if (result == NULL) {
        printf ("escapge tags:malloc\n");
        return NULL;
    }
    memset (result, 0, MAX_TEXT_SIZE);
    FILE *stream = fopen (original, "r");
    if (stream == NULL) {
        printf ("escape tags:fopen\n");
        free (result);
        result = NULL;
        return NULL;
    }
    int c;
    while ((c = fgetc (stream)) != EOF) {
        int found = 1;
        switch (c) {
            case 38:  strcat (result,"&amp;"); break; //& 
            case 198: strcat (result,"&AElig;"); break; //Æ 
            case 193: strcat (result,"&Aacute;"); break; //Á 
            case 194: strcat (result,"&Acirc;"); break; //Â 
            case 192: strcat (result,"&Agrave;"); break; //À 
            case 197: strcat (result,"&Aring;"); break; //Å 
            case 195: strcat (result,"&Atilde;"); break; //Ã 
            case 196: strcat (result,"&Auml;"); break; //Ä 
            case 199: strcat (result,"&Ccedil;"); break; //Ç 
            case 208: strcat (result,"&ETH;"); break; //Ð 
            case 201: strcat (result,"&Eacute;"); break; //É 
            case 202: strcat (result,"&Ecirc;"); break; //Ê 
            case 200: strcat (result,"&Egrave;"); break; //È 
            case 203: strcat (result,"&Euml;"); break; //Ë 
            case 205: strcat (result,"&Iacute;"); break; //Í 
            case 206: strcat (result,"&Icirc;"); break; //Î 
            case 204: strcat (result,"&Igrave;"); break; //Ì 
            case 207: strcat (result,"&Iuml;"); break; //Ï 
            case 209: strcat (result,"&Ntilde;"); break; //Ñ 
            case 211: strcat (result,"&Oacute;"); break; //Ó 
            case 212: strcat (result,"&Ocirc;"); break; //Ô 
            case 210: strcat (result,"&Ograve;"); break; //Ò 
            case 216: strcat (result,"&Oslash;"); break; //Ø 
            case 213: strcat (result,"&Otilde;"); break; //Õ 
            case 214: strcat (result,"&Ouml;"); break; //Ö 
            case 222: strcat (result,"&THORN;"); break; //Þ 
            case 218: strcat (result,"&Uacute;"); break; //Ú 
            case 219: strcat (result,"&Ucirc;"); break; //Û 
            case 217: strcat (result,"&Ugrave;"); break; //Ù 
            case 220: strcat (result,"&Uuml;"); break; //Ü 
            case 221: strcat (result,"&Yacute;"); break; //Ý 
            case 225: strcat (result,"&aacute;"); break; //á 
            case 226: strcat (result,"&acirc;"); break; //â 
            case 230: strcat (result,"&aelig;"); break; //æ 
            case 224: strcat (result,"&agrave;"); break; //à 
            case 229: strcat (result,"&aring;"); break; //å 
            case 227: strcat (result,"&atilde;"); break; //ã 
            case 228: strcat (result,"&auml;"); break; //ä 
            case 231: strcat (result,"&ccedil;"); break; //ç 
            case 233: strcat (result,"&eacute;"); break; //é 
            case 234: strcat (result,"&ecirc;"); break; //ê 
            case 232: strcat (result,"&egrave;"); break; //è 
            case 240: strcat (result,"&eth;"); break; //ð 
            case 235: strcat (result,"&euml;"); break; //ë 
            case 237: strcat (result,"&iacute;"); break; //í 
            case 238: strcat (result,"&icirc;"); break; //î 
            case 236: strcat (result,"&igrave;"); break; //ì 
            case 239: strcat (result,"&iuml;"); break; //ï 
            case 241: strcat (result,"&ntilde;"); break; //ñ 
            case 243: strcat (result,"&oacute;"); break; //ó 
            case 244: strcat (result,"&ocirc;"); break; //ô 
            case 242: strcat (result,"&ograve;"); break; //ò 
            case 248: strcat (result,"&oslash;"); break; //ø 
            case 245: strcat (result,"&otilde;"); break; //õ 
            case 246: strcat (result,"&ouml;"); break; //ö 
            case 223: strcat (result,"&szlig;"); break; //ß 
            case 254: strcat (result,"&thorn;"); break; //þ 
            case 250: strcat (result,"&uacute;"); break; //ú 
            case 251: strcat (result,"&ucirc;"); break; //û 
            case 249: strcat (result,"&ugrave;"); break; //ù 
            case 252: strcat (result,"&uuml;"); break; //ü 
            case 253: strcat (result,"&yacute;"); break; //ý 
            case 255: strcat (result,"&yuml;"); break; //ÿ 
            case 162: strcat (result,"&cent;"); break; //¢ default:
            default: found = 0; break;
        }
        if (!found) {
            //fix me, test when c > 127
            sprintf (result, "%s%d", result, c);
        }
    }
    return result;
}

char *
get_escape_html (const char *html)
{
    char *result = NULL;
    printf ("get escape html, orig\n:%s\n", html);

    if (html == NULL) {
        printf ("get escape html:origin NULL\n");
        return NULL;
    }
    int len = strlen (html) * 2;
    result = (char *) malloc (len);
    if (result == NULL) {
        printf ("get escape html:malloc");
        return NULL;
    }
    memset (result, 0, len);

    char *tmp = html;
    int length = 0;
    int i = 0;
    while (*tmp != '\0') {
        switch (*tmp) {
            case 60: strcat (result, "&lt;"); break;
            case 62: strcat (result, "&gt;"); break;
            case 34: strcat (result, "&quot;"); break;
            case '\n': strcat (result, "<br/>"); break;
            default: length = strlen (result); result[length] = *tmp; result[length + 1] = '\0'; break;
        }
        tmp++;
    }
    printf ("get escape html, escape\n:%s\n", result);

    return result;
}

/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef __FILE_UTIL_H
#define __FILE_UTIL_H

#include <stdint.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>

typedef struct _FileInfo {
    char *name;
    char *type;
    char *path;
    ino_t inode;
    off_t size;
    char *time;
    char *permission;
    char *mime;
    struct _FileInfo *next;
} FileInfo, *FileInfoList;

FileInfoList file_info_list_new (const char *path);

void file_info_list_free (FileInfoList list);

void file_info_list_print (FileInfoList list);

#endif

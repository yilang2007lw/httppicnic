/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef __HPC_LOG_H
#define __HPC_LOG_H

#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <pwd.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>

#define HPC_LOG_DEBUG 0
#define HPC_LOG_INFO  1
#define HPC_LOG_WARN  2
#define HPC_LOG_ERROR 3

#define __BASEFILE__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

int hpc_log_init (const char *path);

int hpc_log_fini ();

void hpc_log (int level, const char *fmt, const char *file, const char *func, int line,  ...);

#define hpc_debug(fmt, ...)     hpc_log (HPC_LOG_DEBUG, fmt, __BASEFILE__, __func__, __LINE__, __VA_ARGS__)
#define hpc_info(fmt, ...)      hpc_log (HPC_LOG_INFO, fmt, __BASEFILE__, __func__, __LINE__, __VA_ARGS__)
#define hpc_warn(fmt, ...)      hpc_log (HPC_LOG_WARN, fmt, __BASEFILE__, __func__, __LINE__, __VA_ARGS__)
#define hpc_error(fmt, ...)     hpc_log (HPC_LOG_ERROR, fmt, __BASEFILE__, __func__, __LINE__,  __VA_ARGS__)

#endif

/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef __HPC_POOL_H
#define __HPC_POLL_H

#include <inttypes.h>
#include <stdlib.h>

typedef unsigned char u_char;

#define HPC_PAGESIZE            4096
#define HPC_POOL_MAX_ALLOC      ((HPC_PAGESIZE) - 1)
#define HPC_POOL_DEFAULT_SIZE   (16 * 1024)

#define HPC_ALIGNMENT 16
#define hpc_align_alignment(d, a)       (((d) + (a - 1)) & ~(a - 1))
#define hpc_align(d)                    hpc_align_alignment(d, HPC_ALIGNMENT)
#define hpc_align_ptr(p, a)             (u_char *) (((uintptr_t) (p) + ((uintptr_t) a - 1)) & ~((uintptr_t) a - 1))

#define hpc_alloc           malloc
#define hpc_calloc          calloc
#define hpc_free            free
#define hpc_memzero(p, s)   memset ((p), 0, (s))

typedef struct hpc_pool_s hpc_pool_t;
typedef struct hpc_pool_large_s hpc_pool_large_t;

struct hpc_pool_large_s {
    hpc_pool_large_t *next;
    void *alloc;
};

typedef struct {
    u_char *last;
    u_char *end;
    hpc_pool_t *next;
} hpc_pool_data_t;

struct hpc_pool_s {
    hpc_pool_data_t d;
    size_t max;
    hpc_pool_t *current;
    hpc_pool_large_t *large;
};

void *hpc_memalign (size_t alignment, size_t size);

hpc_pool_t *hpc_pool_create (size_t size);

void hpc_pool_destroy (hpc_pool_t *pool);

void hpc_pool_reset (hpc_pool_t *pool);

void *hpc_pool_alloc (hpc_pool_t *pool, size_t size);

void *hpc_pool_nalloc (hpc_pool_t *pool, size_t size);

void *hpc_pool_calloc (hpc_pool_t *pool, size_t size);

void hpc_pool_free (hpc_pool_t *pool, void *p);

char *hpc_pool_dup (hpc_pool_t *pool, const char *string);

#endif

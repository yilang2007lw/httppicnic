/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include "file.h"
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <time.h>

static hpc_file_t *hpc_file_new (const char *path, hpc_pool_t *pool);
static char *get_file_time (struct stat *buf, hpc_pool_t *pool);
static char *get_file_type (struct stat *buf, hpc_pool_t *pool);
static char *get_file_permission (struct stat *buf, hpc_pool_t *pool);
static char *get_file_mimetype (const char *path, hpc_pool_t *pool);

hpc_file_list_t *
hpc_file_list_new (const char *path, hpc_pool_t *pool)
{
    hpc_file_list_t* list = NULL;
    
    list = (hpc_file_list_t *) hpc_pool_alloc (pool, sizeof (hpc_file_list_t));
    if (list == NULL) {
        hpc_warn ("%s", "alloc for hpc file list\n");
        return NULL;
    }
    list->pool = pool;
    hpc_file_t *head = hpc_file_new (path, list->pool);
    list->head = head;

    DIR *dir = opendir (path);
    if  (dir != NULL) {

        hpc_file_t *tmp = head;
        struct dirent *dirp;
        while  ((dirp = readdir (dir)) != NULL) {
            if (strcmp (dirp->d_name, ".") == 0 || strcmp (dirp->d_name, "..") == 0){
                continue;
            }

            char *new_path = (char *) hpc_pool_calloc (list->pool, 255);
            strncpy (new_path, path, strlen (path) + 1);
            strncat (new_path, "/", 1);
            strncat (new_path, dirp->d_name, strlen (dirp->d_name));

            hpc_file_t *pfile = hpc_file_new (new_path, list->pool);
            tmp->next = pfile;
            tmp = tmp->next;
        }
    } else {
        hpc_warn ("opendir for %s failed\n", path);
    }

    return list;
}

void 
hpc_file_list_free (hpc_file_list_t* list)
{
    hpc_pool_destroy (list->pool);
}

void 
hpc_file_list_traverse (hpc_file_list_t* list, void *cb (void *arg))
{
    hpc_file_t *tmp = list->head;
    while (tmp->next != NULL) {
        cb (tmp);
        tmp = tmp->next;
    }
}

static hpc_file_t *
hpc_file_new (const char *path, hpc_pool_t *pool)
{
    hpc_file_t *file = NULL;

    if (path == NULL || pool == NULL) {
        hpc_warn ("%s", "path or pool NULL\n");
        return NULL;
    }

    file = (hpc_file_t *) hpc_pool_alloc (pool, sizeof (hpc_file_t));
    if (file == NULL) {
        hpc_warn ("%s", "alloc for hpc_file_t\n");
        return NULL;
    }
    file->name = (char *) hpc_pool_calloc (pool, 255);
    if (file->name != NULL) { 
        strcpy (file->name, strrchr(path, '/') ? strrchr(path, '/') + 1 : path);
    }

    file->path = (char *) hpc_pool_calloc (pool, 255);
    if (file->path != NULL) {
        strcpy (file->path, path);
    }

    struct stat buf;
    if (lstat (path, &buf) == -1) {
        hpc_warn ("%s", "lstat");
    } 
    file->inode = buf.st_ino;
    file->size = buf.st_size;

    file->time = get_file_time (&buf, pool);
    file->type = get_file_type (&buf, pool);
    file->permission = get_file_permission (&buf, pool);
    file->mime = get_file_mimetype (path, pool);

    return file;
}


static char *
get_file_time (struct stat *buf, hpc_pool_t *pool)
{
    if (buf == NULL || pool == NULL) {
        hpc_warn ("%s", "buf or pool NULL\n");
        return NULL;
    }

    char *time =  hpc_pool_calloc (pool, 127);
    if (time == NULL) {
        hpc_warn ("%s", "alloc for time\n");
        return NULL;
    }

    char *rtime = ctime (&buf->st_ctime);
    strncpy (time, rtime, strlen(rtime) + 1);

    return time;
}

static char *
get_file_type (struct stat *buf, hpc_pool_t *pool)
{
    if (buf == NULL || pool == NULL) {
        hpc_warn ("%s", "buf or pool NULL\n");
        return NULL;
    }

    char *type = hpc_pool_calloc (pool, 20);
    if (type == NULL) {
        hpc_warn ("%s", "alloc for file type");
        return NULL;
    }

    if (S_ISDIR (buf->st_mode)) {
        strncpy (type, "directory", 10);
    } else if (S_ISREG (buf->st_mode)) {
        strncpy (type, "regular", 8);
    } else if (S_ISCHR (buf->st_mode)) {
        strncpy (type, "char", 5);
    } else if (S_ISBLK (buf->st_mode)) {
        strncpy (type, "blk", 4);
    } else if (S_ISFIFO (buf->st_mode)) {
        strncpy (type, "fifo", 5);
    } else if (S_ISLNK (buf->st_mode)) {
        strncpy (type, "link", 5);
    } else if (S_ISSOCK (buf->st_mode)) {
        strncpy (type, "socket", 7);
    } else {
        strncpy (type, "unknown", 8);    
    }

    return type;
}

static char *
get_file_permission (struct stat *buf, hpc_pool_t *pool)
{
    if (buf == NULL || pool == NULL) {
        hpc_warn ("%s", "buf or pool NULL\n");
        return NULL;
    }

    char *permission = hpc_pool_calloc (pool, 10);
    if (permission == NULL) {
        hpc_warn ("%s", "alloc for file permission\n");
        return NULL;
    }

    if (S_IRUSR & buf->st_mode) {
        strncat (permission, "r", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IWUSR & buf->st_mode) { 
        strncat (permission, "w", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IXUSR & buf->st_mode) {
        strncat (permission, "x", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IRGRP & buf->st_mode) {
        strncat (permission, "r", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IWGRP & buf->st_mode) {
        strncat (permission, "w", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IXGRP & buf->st_mode) {
        strncat (permission, "x", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IROTH & buf->st_mode) {
        strncat (permission, "r", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IWOTH & buf->st_mode) {
        strncat (permission, "w", 1);
    } else {
        strncat (permission, "-", 1);
    }
    if (S_IXOTH & buf->st_mode) {
        strncat (permission, "x", 1);
    } else {
        strncat (permission, "-", 1);
    }

    return permission;
}

static char *
get_file_mimetype (const char *path, hpc_pool_t *pool)
{
    if (path == NULL || pool == NULL) {
        hpc_warn ("%s", "path or pool NULL\n");
        return NULL;
    }

    char *mime = hpc_pool_calloc (pool, 20);
    if (mime == NULL) {
        hpc_warn ("%s", "alloc for mime\n");
        return NULL;
    }

    char *command = hpc_pool_calloc (pool, 255);
    if (command == NULL) {
        hpc_warn ("%s", "alloc for command\n");
        return NULL;
    }
    snprintf (command, 256, "%s %s", "file -i ", path);

    char *output = hpc_pool_calloc (pool, 1024);
    if (output == NULL) {
        hpc_warn ("%s", "alloc for output");
        return NULL;
    }

    FILE *stream = popen (command, "r");
    if (stream == NULL) {
        hpc_warn ("%s", "popen command");
        return NULL;
    }

    if (fgets (output, 1024, stream) == NULL) {
        hpc_warn ("%s", "fgets");
    }

    char *mime_charset = strrchr (output, ':');
    if (mime_charset == NULL) {
        hpc_warn ("%s", "get mime charset index");
        return NULL;
    }

    char *charset = strrchr (mime_charset, ';');
    if (charset == NULL) {
        hpc_warn ("%s", "get charset index");
        return NULL;
    }
    mime_charset++;
    mime_charset++;

    strncpy (mime, mime_charset, strlen (mime_charset) - strlen (charset));

    pclose (stream);

    return mime;
}

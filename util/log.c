/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include "log.h"

#define HPC_LOG_DEFAULT_PATH "/tmp/hpc.log"

static FILE *hpc_log_s = NULL;

int 
hpc_log_init (const char *path)
{
    if (hpc_log_s != NULL) {
        fprintf (stderr, "hpc_log_init:%s\n", "hpc_log_s not NULL");
        return 1;
    }

    if (path != NULL) {
        hpc_log_s = fopen (path, "w");
    } else {
        hpc_log_s = fopen (HPC_LOG_DEFAULT_PATH, "w");
    }
    if (hpc_log_s == NULL) {
        fprintf (stderr, "hpc_log_init:%s\n", "fopen");
    }
    return 0;
}

int 
hpc_log_fini ()
{
    if (hpc_log_s == NULL) {
        fprintf (stderr, "hpc_log_fini:%s\n", "hpc_log_s NULL");
        return 1;
    }
    return fclose (hpc_log_s);
}

void 
hpc_log (int level, const char *fmt, const char *file, const char *func, int line, ...)
{
    if (hpc_log_s == NULL) {
        hpc_log_init (NULL);
    }
    const char *name = NULL;
    struct passwd *pwd;
    uid_t uid = getuid ();
    pwd = getpwuid (uid);
    if (pwd != NULL) {
        name = pwd->pw_name;
    }

    time_t now = time (NULL);
    char *time = strtok (ctime (&now), "\n");

    switch (level) {
        case 0:
            fprintf (hpc_log_s, "%s %s %s[%d]:<%s> %s: ", time, name, file, line, "debug", func);
            break;
        case 1:
            fprintf (hpc_log_s, "%s %s %s[%d]:<%s> %s: ", time, name, file, line, "info", func);
            break;
        case 2:
            fprintf (hpc_log_s, "%s %s %s[%d]:<%s> %s: ", time, name, file, line, "warn", func);
            break;
        case 3:
            fprintf (hpc_log_s, "%s %s %s[%d]:<%s> %s: ", time, name, file, line, "error", func);
            break;
        default:
            break;
    }
    va_list ap;
    va_start (ap, line);
    vfprintf (hpc_log_s, fmt, ap);
    va_end (ap);
    fprintf (hpc_log_s, "\n");
}

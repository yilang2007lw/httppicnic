/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef __HPC_FILE_H
#define __HPC_FILE_H

#include <stdint.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include "pool.h"
#include "log.h"

typedef struct hpc_file_s       hpc_file_t;
typedef struct hpc_file_list_s  hpc_file_list_t;

struct hpc_file_s {
    char *name;
    char *type;
    char *path;
    ino_t inode;
    off_t size;
    char *time;
    char *permission;
    char *mime;
    hpc_file_t *next;
};

struct hpc_file_list_s {
    hpc_file_t *head;
    hpc_pool_t *pool;
};

hpc_file_list_t* hpc_file_list_new (const char *path, hpc_pool_t *pool);

void hpc_file_list_free (hpc_file_list_t* list);

void hpc_file_list_traverse (hpc_file_list_t* list, void *cb (void *arg));

#endif

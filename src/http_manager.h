/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef __HTTP_MANAGER_H
#define __HTTP_MANAGER_H

#include "file_manager.h"

void default_cb (struct evhttp_request *req, void *arg);

void index_cb (struct evhttp_request *req, void *arg);

void static_cb (struct evhttp_request *req, void *arg);

void resource_cb (struct evhttp_request *req, void *arg);

void textfile_cb (struct evhttp_request *req, void *arg);

void imagefile_cb (struct evhttp_request *req, void *arg);

void videofile_cb (struct evhttp_request *req, void *arg);

void file_manager_cb (struct evhttp_request *req, void *arg);

#endif

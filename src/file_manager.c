/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file_manager.h"

#define FILE_MANAGER_HTML_PATH  "../static/filemanager.html"
#define FILE_MANAGER_CSS_PATH   "../static/filemanager.css"
#define FILE_MANAGER_JS_PATH    "../static/filemanager.js"
#define TEXTFILE_HTML_PATH      "../static/textfile.html"
#define IMAGEFILE_HTML_PATH     "../static/imagefile.html"
#define VIDEOFILE_HTML_PATH     "../static/videofile.html"

void 
handle_file_manager (const char *path)
{
    if (path == NULL) {
        perror ("handle file manager:path is NULL");
        return ;
    }

    HtmlNodeTree html, head, title, meta, body;

    html = html_node_tree_new ("html", NULL, NULL);
    head = html_node_tree_new ("head", NULL, html);
    title = html_node_tree_new ("title", NULL, head); 
    meta = html_node_tree_new ("meta", NULL, head);
    html_node_tree_set_attribute (meta, "http-equiv", "content-type");
    html_node_tree_set_attribute (meta, "content", "text/html;charset=utf-8");

    body = html_node_tree_new ("body", NULL, html);
    if (html == NULL || head == NULL || title == NULL || body == NULL) {
        perror ("handle file manager:create basic html node failed");
        return ;
    }
    html_node_tree_set_text (title, path);

    FileInfoList  list = file_info_list_new (path);
    if (list == NULL) {
        perror ("handle file manager:file info list NULL");
        return ;
    }

    HtmlNodeTree table;
    table = html_node_tree_new ("table", NULL, body);
    if (table == NULL) {
        perror ("handle file manager:table");
        return ;
    }

    FileInfo *p = list;
    HtmlNodeTree tr, name, type, size, time, permission, mime, link;

    tr = html_node_tree_new ("tr", NULL, table);

    name = html_node_tree_new ("th", NULL, tr);
    html_node_tree_set_text (name, "name");

    type = html_node_tree_new ("th", NULL, tr);
    html_node_tree_set_text (type, "type");

    mime = html_node_tree_new ("th", NULL, tr);
    html_node_tree_set_text (mime, "mime");

    size = html_node_tree_new ("th", NULL, tr);
    html_node_tree_set_text (size, "size");

    permission = html_node_tree_new ("th", NULL, tr);
    html_node_tree_set_text (permission, "permission"); 

    time = html_node_tree_new ("th", NULL, tr);
    html_node_tree_set_text (time, "time");

    while (p->next != NULL) {
        tr = html_node_tree_new ("tr", NULL, table);

        name = html_node_tree_new ("td", NULL, tr);
        if (p->mime != NULL) {
            link = html_node_tree_new ("a", p->name, name);
            char *href = (char *)malloc (256);
            if (href == NULL) {
                printf ("textfile malloc");
            }
            memset (href, 0, 256);
            if (strncmp (p->mime, "text/", 5) == 0) {
                sprintf (href, "/textfile?path=%s", p->path);
            } else if (strncmp (p->mime, "image/", 6) == 0) {
                sprintf (href, "/imagefile?path=%s", p->path);
            } else if (strncmp (p->mime, "video/", 6) == 0) { 
                sprintf (href, "/videofile?path=%s", p->path);
            } else {
                sprintf (href, "/filemanager?path=%s", p->path);
            }
            html_node_tree_set_attribute (link, "href", href);
            free (href);
            href = NULL;
        } else {
            html_node_tree_set_text (name, p->name);
        }

        type = html_node_tree_new ("td", NULL, tr);
        html_node_tree_set_text (type, p->type);

        mime = html_node_tree_new ("td", NULL, tr);
        html_node_tree_set_text (mime, p->mime);

        size = html_node_tree_new ("td", NULL, tr);
        char *size_str = malloc (256);
        if (size_str == NULL) {
            perror ("malloc in while");
            continue;
        }
        memset (size_str, 0, 256);
        sprintf (size_str, "%jd",(intmax_t) p->size);
        html_node_tree_set_text (size, size_str);
        free (size_str);
        size_str = NULL;

        permission = html_node_tree_new ("td", NULL, tr);
        html_node_tree_set_text (permission, p->permission);

        time = html_node_tree_new ("td", NULL, tr);
        html_node_tree_set_text (time, p->time);

        p = p->next;
    }

    html_node_tree_inject_css (html, FILE_MANAGER_CSS_PATH);
    html_node_tree_inject_js (title, FILE_MANAGER_JS_PATH);
    write_node_tree_to_html (html, FILE_MANAGER_HTML_PATH);

    file_info_list_free (list);
    html_node_tree_free (html);
}

void 
handle_textfile (const char *path)
{
    if (path == NULL) {
        perror ("handle imagefile:path is NULL");
        return ;
    }

    HtmlNodeTree html, head, title, meta, body, container, pre;

    html = html_node_tree_new ("html", NULL, NULL);
    head = html_node_tree_new ("head", NULL, html);
    title = html_node_tree_new ("title", path, head); 
    meta = html_node_tree_new ("meta", NULL, head);
    html_node_tree_set_attribute (meta, "http-equiv", "content-type");
    html_node_tree_set_attribute (meta, "content", "text/html;charset=utf-8");
    body = html_node_tree_new ("body", NULL, html);
    if (html == NULL || head == NULL || title == NULL || body == NULL) {
        perror ("handle textfile:create basic html node failed");
        return ;
    }
    html_node_tree_set_text (title, "This is title!");
    container = html_node_tree_new ("div", NULL, body);
    pre = html_node_tree_new ("pre", NULL, container);
    char *content = (char *) malloc (65536);
    if (content == NULL) {
        perror ("handle textfile:malloc");
        html_node_tree_free (html);
        return ;
    }
    memset (content, 0, 65536);
    FILE *stream = fopen (path, "r");
    if (stream == NULL) {
        perror ("handle textfile:fopen");
        free (content);
        content = NULL;
        html_node_tree_free (html);
        return ;
    }
    fread ((void *) content, 1, 65536, stream);
    fclose (stream);

    char *escape_content = get_escape_html (content);
    free (content);
    content = NULL;

    html_node_tree_set_text (pre, escape_content);
    free (escape_content);
    escape_content = NULL;

    write_node_tree_to_html (html, TEXTFILE_HTML_PATH);
    html_node_tree_free (html);
}

void
handle_imagefile (const char *path)
{
    if (path == NULL) {
        perror ("handle imagefile:path is NULL");
        return ;
    }

    HtmlNodeTree html, head, title, meta, body, container, img;

    html = html_node_tree_new ("html", NULL, NULL);
    head = html_node_tree_new ("head", NULL, html);
    title = html_node_tree_new ("title", path, head); 
    meta = html_node_tree_new ("meta", NULL, head);
    html_node_tree_set_attribute (meta, "http-equiv", "content-type");
    html_node_tree_set_attribute (meta, "content", "text/html;charset=utf-8");
    body = html_node_tree_new ("body", NULL, html);
    if (html == NULL || head == NULL || title == NULL || body == NULL) {
        perror ("handle imagefile:create basic html node failed");
        return ;
    }
    html_node_tree_set_text (title, "This is title!");
    container = html_node_tree_new ("div", NULL, body);
    img = html_node_tree_new ("img", NULL, container);
    char *src = (char *) malloc (256);
    if (src == NULL) {
        perror ("malloc");
        html_node_tree_free (html);
        return;
    }
    memset (src, 0, 256);
    sprintf (src, "/resource?path=%s", path);
    html_node_tree_set_attribute (img, "src", src);
    html_node_tree_set_attribute (img, "width", "auto");
    html_node_tree_set_attribute (img, "height", "auto");

    write_node_tree_to_html (html, IMAGEFILE_HTML_PATH);
    free (src);
    src = NULL;
    html_node_tree_free (html);
}

void
handle_videofile (const char *path)
{
    if (path == NULL) {
        perror ("handle imagefile:path is NULL");
        return ;
    }

    HtmlNodeTree html, head, title, meta, body, container, video;

    html = html_node_tree_new ("html", NULL, NULL);
    head = html_node_tree_new ("head", NULL, html);
    title = html_node_tree_new ("title", path, head); 
    meta = html_node_tree_new ("meta", NULL, head);
    html_node_tree_set_attribute (meta, "http-equiv", "content-type");
    html_node_tree_set_attribute (meta, "content", "text/html;charset=utf-8");
    body = html_node_tree_new ("body", NULL, html);
    if (html == NULL || head == NULL || title == NULL || body == NULL) {
        perror ("handle imagefile:create basic html node failed");
        return ;
    }
    html_node_tree_set_text (title, "This is title!");
    container = html_node_tree_new ("div", NULL, body);
    video = html_node_tree_new ("video", NULL, container);
    char *src = (char *) malloc (256);
    if (src == NULL) {
        perror ("malloc");
        html_node_tree_free (html);
        return;
    }
    memset (src, 0, 256);
    sprintf (src, "/resource?path=%s", path);
    html_node_tree_set_attribute (video, "src", src);
    html_node_tree_set_attribute (video, "controls", "controls");
    html_node_tree_set_text (video, "Your browse doesn't support html5 video");
    html_node_tree_set_attribute (video, "width", "auto");
    html_node_tree_set_attribute (video, "height", "auto");

    write_node_tree_to_html (html, VIDEOFILE_HTML_PATH);
    free (src);
    src = NULL;
    html_node_tree_free (html);
}

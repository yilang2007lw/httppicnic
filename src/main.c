/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <event2/event.h>
#include <event2/buffer.h>
#include <event2/http.h>
#include "http_manager.h"
#include "http_util.h"

int port = -1;
char *ip = NULL;
char *root = NULL;

struct option longopts[] = {
    {"ip", required_argument, NULL, 'i'},
    {"port", required_argument, NULL, 'p'},
    {"root", required_argument, NULL, 'r'},
    {"Debug", optional_argument, NULL, 'D'},
    {"daemon", no_argument, NULL, 'd'},
    { 0, 0, 0, 0 },
};

void 
dispatch_url (struct evhttp_request *req, void *arg)
{
    const char *uri = evhttp_request_get_uri (req);
    printf ("request uri:%s\n", uri);

    char *func = get_handle_func_for_url (uri);

    if (strcmp (func, "index") == 0) {
        index_cb (req, arg);
    } else if (strcmp (func, "static") == 0) {
        static_cb (req, arg);
    } else if (strcmp (func, "resource") == 0) {
        resource_cb (req, arg);
    } else if (strcmp (func, "textfile") == 0) {
        textfile_cb (req, arg);
    } else if (strcmp (func, "imagefile") == 0) {
        imagefile_cb (req, arg);
    } else if (strcmp (func, "videofile") == 0) {
        videofile_cb (req, arg);
    } else if (strcmp (func, "filemanager") == 0) {
        file_manager_cb (req, arg);
    } else {
        default_cb (req, arg);
    }

    free (func);
    func = NULL;
}

static void
daemonize ()
{
    printf ("run in daemonize mode\n");
}

static void
set_debug_mode ()
{
    printf ("set debug mode\n");
}

int main (int argc, char **argv)
{
    struct event_base *base = NULL;
    struct evhttp *evhttp = NULL;

    base = event_base_new ();
    if (base == NULL) {
        fprintf (stderr,"event base new failed\n");
        return 1;
    }

    evhttp = evhttp_new (base);
    if (evhttp == NULL) {
        fprintf (stderr,"evhttp new failed\n");
        return 1;
    }

    evhttp_set_gencb (evhttp, dispatch_url, NULL);

    int c;
    while ((c = getopt_long(argc, argv, ":i:p:r:D:d", longopts, NULL)) != -1) {
        switch (c) {
        case 'i':
            ip = strdup (optarg);
            break;
        case 'p':
            port = atoi (optarg);
            break;
        case 'r':
            root = strdup (optarg);
            break;
        case 'D':
            set_debug_mode ();
            break;
        case 'd':
            daemonize ();
            break;
        default:
            printf ("unknown option\n");
            break;
        }
    }

    if (ip == NULL) {
        ip = get_server_address ();
    }
    if (port == -1) {
        port = get_server_port ();
    }
    if (root == NULL) {
        root = get_user_home ();
    }

    evhttp_bind_socket (evhttp, ip, port);
    event_base_dispatch (base);

    free (ip);
    ip = NULL;
    free (root);
    root = NULL;
    evhttp_free (evhttp);
    event_base_free (base);

    return 0;
}

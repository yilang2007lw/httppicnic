/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <event2/event.h>
#include <event2/buffer.h>
#include <event2/http.h>
#include "http_manager.h"

#define INDEX_HTML_PATH         RESOURCE_DIR"index.html"
#define FILE_MANAGER_HTML_PATH  RESOURCE_DIR"filemanager.html"
#define TEXTFILE_HTML_PATH      RESOURCE_DIR"textfile.html"
#define IMAGEFILE_HTML_PATH     RESOURCE_DIR"imagefile.html"
#define VIDEOFILE_HTML_PATH     RESOURCE_DIR"videofile.html"

extern char *root;

static void
response_file (struct evhttp_request *req, const char *path)
{
    struct evbuffer *evb;
    struct stat buf;

    if (req == NULL || path == NULL) {
        fprintf (stderr, "req or path is NULL\n");
        goto out;
    }
    evb = evbuffer_new ();
    if (evb == NULL) {
        fprintf (stderr, "response file:evbuffer_new\n");
        goto out;
    }
    int fd = open (path, O_RDONLY); 
    if (fd < 0) {
        fprintf (stderr, "response file:open\n");
        goto out;
    }
    if (stat (path, &buf) < 0) {
        fprintf (stderr, "response file:stat\n");
        goto out;
    }
    if (buf.st_size > 0) {
        if (evbuffer_add_file (evb, fd, 0, buf.st_size) < 0) {
            fprintf (stderr, "response file:evbuffer_add_file\n");
            goto out;
        }
    }
    evhttp_send_reply (req, 200, "ok", evb);

    evbuffer_free (evb);
out:
    evhttp_send_reply (req, 200, "ok", NULL);
    //if (evb != NULL) {
    //    evbuffer_free (evb);
    //}
}

void 
default_cb (struct evhttp_request *req, void *arg)
{
    const char *uri = evhttp_request_get_uri (req);

    struct evbuffer *evb = evbuffer_new ();
    if (evb == NULL) {
        fprintf (stderr, "default cb:evbuffer_new\n");
    }
    evbuffer_add_printf (evb, "Hello, you requested %s", uri);
    evbuffer_add_printf (evb, "<p><a href=\"index.html\">click to index</a></p>");

    evhttp_send_reply (req, 200, "ok", evb);
    evbuffer_free (evb);
}

void 
index_cb (struct evhttp_request *req, void *arg)
{
    response_file (req, INDEX_HTML_PATH);
}

void
static_cb (struct evhttp_request *req, void *arg)
{
    const char *uri = evhttp_request_get_uri (req);
    char *p = strstr (uri, "/static/");
    if (p == NULL) {
        fprintf (stderr, "static cb:%s\n", uri);
    }
    p = p + 8;
    char *path = (char *) malloc (256);
    if (path == NULL) {
        fprintf (stderr, "static cb malloc\n");
    }
    memset (path, 0, 256);

    sprintf (path, "%s%s", RESOURCE_DIR, p);
    response_file (req, path);
}

void 
resource_cb (struct evhttp_request *req, void *arg)
{
    const char *uri = evhttp_request_get_uri (req);
    char *path = get_query_value (uri, "path");
    response_file (req, path);
    free (path);
    path = NULL;
}

void 
textfile_cb (struct evhttp_request *req, void *arg)
{
    const char *uri = evhttp_request_get_uri (req);
    char *path = get_query_value (uri, "path");
    handle_textfile (path);
    free (path);
    path = NULL;
    response_file (req,  TEXTFILE_HTML_PATH);
}

void
imagefile_cb (struct evhttp_request *req, void *arg)
{
    const char *uri = evhttp_request_get_uri (req);
    char *path = get_query_value (uri, "path");
    handle_imagefile (path);
    free (path);
    path = NULL;
    response_file (req,  IMAGEFILE_HTML_PATH);
}

void
videofile_cb (struct evhttp_request *req, void *arg)
{
    const char *uri = evhttp_request_get_uri (req);
    char *path = get_query_value (uri, "path");
    handle_videofile (path);
    free (path);
    path = NULL;
    response_file (req,  VIDEOFILE_HTML_PATH);
}

void 
file_manager_cb (struct evhttp_request *req, void *arg) 
{
    //fix me, handle file path not exists or file unreadable
    const char *uri = evhttp_request_get_uri (req);
    char *path = get_query_value (uri, "path");
    if (path == NULL) {
        path = strdup (root);
    }
    handle_file_manager (path);
    free (path);
    path = NULL;
    response_file (req, FILE_MANAGER_HTML_PATH);
}

/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include <unistd.h>
#include <stdio.h>
#include "file_util.h"
#include "html_util.h"

static void
test_file_util ()
{
    FileInfoList l = file_info_list_new ("/home/longwei");
    file_info_list_print (l);
    file_info_list_free (l);
}

static void
test_get_file_mimetype ()
{
    //printf ("mime:%s\n", get_file_mimetype ("/home/longwei"));
    ;
}

static void
test_html_node ()
{
    HtmlNodeTree html = html_node_tree_new ("html", NULL, NULL);
    HtmlNodeTree head = html_node_tree_new ("head", NULL, html);

    HtmlNodeTree title = html_node_tree_new ("title", NULL, head); 
    //html_node_tree_set_text (title, "This is title");
    html_node_tree_set_attribute (title, "id", "title");

    HtmlNodeTree body = html_node_tree_new ("body", NULL, html);
    html_node_tree_set_klass (body, "body");

    HtmlNodeTree content = html_node_tree_new ("div", NULL, body);
    html_node_tree_set_attribute (content, "id", "content");
    html_node_tree_set_attribute (content, "class", "content");
    html_node_tree_set_id (content, "MyContent");
    html_node_tree_set_text (content, "This is content");
    //html_node_tree_add_klass (content, "BigClassaaaaia");
    html_node_tree_add_klass (content, "Big");
    html_node_tree_add_klass (content, "Pig1");
    html_node_tree_add_klass (content, "Pig1");
    html_node_tree_add_klass (content, "Big2");
    html_node_tree_remove_klass (content, "Big");
   // html_node_tree_add_klass (content, "Su12345678910");
    html_node_tree_set_attribute (content, "class111", "class111");
    //html_node_tree_set_klass (content, "SuperClass");

    html_node_tree_inject_css (content, "main.css");

    html_node_tree_inject_js (content, "1.js");
    html_node_tree_inject_js (title, "2.js");
    //html_node_tree_print (html);
    write_node_tree_to_html (html, "test.html");
    html_node_tree_free (html);
}

static void
test_search_node ()
{
    HtmlNodeTree html = html_node_tree_new ("html", NULL, NULL);
    HtmlNodeTree head = html_node_tree_new ("head", NULL, html);
    HtmlNodeTree title = html_node_tree_new ("title", NULL, head); 
    HtmlNodeTree body = html_node_tree_new ("body", NULL, html);
    HtmlNodeTree content = html_node_tree_new ("div", NULL, body);
    HtmlNodeTree div1 = html_node_tree_new ("div", NULL, content);
    HtmlNodeTree div2 = html_node_tree_new ("div", NULL, content);
    HtmlNodeTree div3 = html_node_tree_new ("div", NULL, content);
    HtmlNodeTree div4 = html_node_tree_new ("div", NULL, content);
    HtmlNodeTree div5 = html_node_tree_new ("div", NULL, content);

    HtmlNodeTree div6 = html_node_tree_new ("div", NULL, div2);
    HtmlNodeTree div7 = html_node_tree_new ("div", NULL, div3);
    HtmlNodeTree div8 = html_node_tree_new ("div", NULL, div3);

    HtmlNodeTree div9 = html_node_tree_new ("div", NULL, div6);
    html_node_tree_set_attribute (div9, "class", "div9");
    html_node_tree_set_attribute (div9, "id", "div9");

    HtmlNodeTree div10 = html_node_tree_new ("div", NULL, div8);
    html_node_tree_set_attribute (div10, "id", "div10");
    html_node_tree_set_attribute (div10, "class", "div10");

    HtmlNodeTree s1 = html_node_tree_find_by_id (html, "div9");
    printf ("s1 class:%s\n", html_node_tree_get_attribute (s1, "klass"));

    HtmlNodeTree s2 = html_node_tree_find_by_klass (body, "div10");
    printf ("s2 id:%s\n", html_node_tree_get_attribute (s2, "id"));

    html_node_tree_free (html);
}

void
run_test (int times) 
{
    while (times > 0) {

        //test_file_util ();
        //test_get_file_mimetype ();
        //test_html_node (); 

        test_search_node ();
        sleep (1);
        times--;
    }
}

int main (int argc, char **argv)
{
    run_test (1);
    //run_test (15);

    return 0;
}

/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include "pool.h"

int main (int argc, char **argv)
{
    hpc_pool_t *pool = hpc_pool_create (4095);
    void *p1 = hpc_pool_alloc (pool, 1024);
    void *p2 = hpc_pool_alloc (pool, 1000);
    void *p3 = hpc_pool_nalloc (pool, 999);
    void *p4 = hpc_pool_alloc (pool, 8000);
    hpc_pool_destroy (pool);
    return 0;
}

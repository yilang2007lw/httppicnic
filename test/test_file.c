/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include "file.h"

void* print_file_name (void *arg)
{
    hpc_file_t *file = (hpc_file_t *)arg;
    printf ("%s\n", file->name);
    return NULL;
}

int main (int argc, char **argv)
{
    hpc_pool_t *pool = hpc_pool_create (4095);

    hpc_file_list_t *filelist = hpc_file_list_new ("/home/longwei/project", pool);
    hpc_file_list_traverse (filelist, print_file_name);

    hpc_file_list_free (filelist);

    return 0;
}

/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include "json.h"

void test1 (hpc_pool_t *pool)
{
    hpc_json_string ("hello world", pool);
    hpc_json_int (8, pool);
    hpc_json_double (9.9, pool);
    hpc_json_object (pool);
    hpc_json_array (pool);
    hpc_json_true (pool);
    hpc_json_false (pool);
    hpc_json_null (pool);
}

void test2 (hpc_pool_t *pool)
{
    hpc_json_t *obj = hpc_json_object (pool);

    hpc_json_t *string = hpc_json_string ("hello world", pool);
    hpc_json_t *intval = hpc_json_int (8, pool);
    hpc_json_t *doubleval = hpc_json_double (9.9, pool);
    hpc_json_t *tt = hpc_json_true (pool);
    hpc_json_t *ff = hpc_json_false (pool);
    hpc_json_t *nl = hpc_json_null (pool);

    hpc_json_object_insert (obj, "string", string);
    hpc_json_object_insert (obj, "intval", intval);
    hpc_json_object_insert (obj, "doubleval", doubleval);
    hpc_json_object_insert (obj, "true", tt);
    hpc_json_object_insert (obj, "false", ff);
    hpc_json_object_insert (obj, "null", nl);

}

int main (int argc, char **argv)
{
    hpc_pool_t *pool = hpc_pool_create (4095);

    //test1 (pool);

    hpc_pool_destroy (pool);

    return 0;
}

/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include "log.h"

void test1 ()
{
    hpc_info ("my info:%s and my age:%d\n", "test", 45);
    hpc_warn ("my info:%s and my age:%d\n", "test", 45);
    hpc_debug ("my info:%s and my age:%d\n", "test", 45);
    hpc_error ("my info:%s and my age:%d\n", "test", 45);
}

int main (int argc, char **argv)
{
    hpc_log_init ("/tmp/mylog.log");

    test1 ();

    hpc_log_fini ();

    return 0;
}

/**
 * Copyright (c) 2013 Long Wei
 *
 * Author:      Long Wei <yilang2007lw@gmail.com>
 * Maintainer:  Long Wei <yilang2007lw@gamil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 **/

#include "html.h"
#include <assert.h>

void *
print_node_tag (void *arg)
{
    hpc_html_node_t *node = (hpc_html_node_t *) arg;
    fprintf (stderr, "%s\n", node->tag);
    return NULL;
}

int main (int argc, char **argv)
{
    hpc_log_init ("/tmp/mylog.log");

    hpc_pool_t *pool = hpc_pool_create (4095);

    hpc_html_tree_t *tree = hpc_html_tree_new ("Hello World", pool);
    
    hpc_html_node_t *d1 = hpc_html_node_new ("div", NULL, tree->body, tree);
    assert (tree->body != NULL);

    hpc_html_node_t *d2 = hpc_html_node_new ("div", NULL, tree->body, tree);
    hpc_html_node_t *d3 = hpc_html_node_new ("div", NULL, tree->body, tree);
    hpc_html_node_t *d4 = hpc_html_node_new ("div", NULL, tree->body, tree);
    hpc_html_node_t *d5 = hpc_html_node_new ("div", NULL, tree->body, tree);
    
    hpc_html_node_t *d6 = hpc_html_node_new ("div", NULL, d1, tree);
    hpc_html_node_t *d7 = hpc_html_node_new ("div", NULL, d1, tree);
    hpc_html_node_t *d8 = hpc_html_node_new ("div", NULL, d3, tree);
    hpc_html_node_t *d9 = hpc_html_node_new ("div", NULL, d8, tree);
    hpc_html_node_t *d10 = hpc_html_node_new ("div", NULL, d9,tree);

    //fprintf (stderr, "d10 depth:%d\n", hpc_html_node_get_depth (d10));
    //fprintf (stderr, "d9 depth:%d\n", hpc_html_node_get_depth (d9));
    //fprintf (stderr, "d8 depth:%d\n", hpc_html_node_get_depth (d8));
    //fprintf (stderr, "d7 depth:%d\n", hpc_html_node_get_depth (d7));
    //fprintf (stderr, "d6 depth:%d\n", hpc_html_node_get_depth (d6));
    //fprintf (stderr, "d5 depth:%d\n", hpc_html_node_get_depth (d5));
    //fprintf (stderr, "d4 depth:%d\n", hpc_html_node_get_depth (d4));
    //fprintf (stderr, "d3 depth:%d\n", hpc_html_node_get_depth (d3));
    //fprintf (stderr, "d2 depth:%d\n", hpc_html_node_get_depth (d2));
    //fprintf (stderr, "d1 depth:%d\n", hpc_html_node_get_depth (d1));

    //fprintf (stderr, "title depth:%d\n", hpc_html_node_get_depth (tree->title));
    //fprintf (stderr, "head depth:%d\n", hpc_html_node_get_depth (tree->head));
    //fprintf (stderr, "body depth:%d\n", hpc_html_node_get_depth (tree->body));
    //fprintf (stderr, "html depth:%d\n", hpc_html_node_get_depth (tree->html));

    hpc_html_node_set_id (d1, "d1");
    hpc_html_node_set_id (d2, "d2");
    hpc_html_node_set_id (d3, "d3");
    hpc_html_node_set_id (d4, "d4");
    hpc_html_node_set_id (d5, "d5");
    hpc_html_node_set_id (d6, "d6");
    hpc_html_node_set_id (d7, "d7");
    hpc_html_node_set_id (d8, "d8");
    hpc_html_node_set_id (d9, "d9");
    hpc_html_node_set_id (d10, "d10");

    hpc_html_node_add_klass (d3, "class1");
    hpc_html_node_add_klass (d3, "class2");
    hpc_html_node_add_klass (d3, "class3");
    hpc_html_node_remove_klass (d3, "class2"); 
    hpc_html_node_remove_klass (d3, "class4"); 

    hpc_html_node_set_attribute (d4, "background", "red");
    hpc_html_node_delete_attribute (d4, "border");

    hpc_html_node_set_klass (d5, "class5");
    hpc_warn ("d5 class:%s", hpc_html_node_get_attribute (d5, "class"));
    hpc_html_node_set_id (d5, "d5");
    hpc_warn ("d5 id:%s", hpc_html_node_get_attribute (d5, "id"));
    hpc_html_node_set_attribute (d5, "border", "1px");
    hpc_warn ("d5 border:%s", hpc_html_node_get_attribute (d5, "border"));

    hpc_html_node_set_text (d10, "This is d 10");

    hpc_html_tree_inject_css (tree, "test.css");
    hpc_html_tree_inject_js (tree, "test.js");

    //hpc_html_node_traverse_bfs (tree->html, print_node_tag);

    hpc_html_tree_write_file (tree, "/tmp/test.html");

    hpc_html_tree_free (tree);

    hpc_log_fini ();

    return 0;
}
